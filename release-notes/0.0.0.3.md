## Version 0.0.0.3

*   Bug fixes:
    *   Fix various bugs to do with shell history
*   Debugging support:
    *   Add a tool to generate symbols to use in gdb
    *   Add support for `backtrace`
*   Libraries:
    *   `term.asm`:
        *   Start of a terminal window concept
        *   Allows moving and resizing
        *   Allows scrolling a viewport
        *   Support for some ANSI codes
        *   Some parts refactored from the old `vga` driver
    *   `readkey.asm`:
        *   Library to process ANSI escape codes
*   Drivers:
    *   Refactor `vga` into `vga_text` and new `term` library

A video introducing this release is available at: https://www.youtube.com/watch?v=UBSHITg\_NlA
