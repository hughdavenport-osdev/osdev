#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

OFFSET_X=2
OFFSET_Y=22
OFFSET_X_RIGHT=2
OFFSET_Y_BOTTOM=26
if [ $# -ne 0 ] && [ "$1" = "serenity" ]; then
    OFFSET_X=0
    OFFSET_Y=0
    OFFSET_X_RIGHT=0
    OFFSET_Y_BOTTOM=0
fi
WIDTH=$((1920 - OFFSET_X - OFFSET_X_RIGHT))
HEIGHT=$((1080 - OFFSET_Y - OFFSET_Y_BOTTOM))
FRAMERATE=30
#PULSE_INPUT_ID=$(pactl list short sources | grep "$(pactl info | grep 'Default Source' | cut -d ':' -f 2 | tr -d ' ')" | cut -f 1)

# TODO better arg parsing
#MONITOR_MODE=1
#PULSE_MONITOR_ID=$(pactl list short sources | grep "$(pactl info | grep 'Default Sink' | cut -d ':' -f 2 | tr -d ' ')" | cut -f 1)

if [ $# -ne 0 ]; then if [ "$1" = "webcam" ] || [ "$1" = "serenity" ]; then
    USE_WEBCAM=1
    WEBCAM_WIDTH=320
    WEBCAM_HEIGHT=180
    WEBCAM_WIDTH=480
    WEBCAM_HEIGHT=270
fi; fi

setup_i3() {
    echo "Setting up workspace"
    i3-msg mark current >/dev/null
    # i3-msg focus output next >/dev/null
    # i3-msg mark active
    for i in $(seq 1 10); do
        i3-msg workspace "$i" >/dev/null
        i3-msg move workspace to output nonprimary >/dev/null
    done
    # i3-msg '[con_mark=active]' focus
    # i3-msg unmark active >/dev/null
    i3-msg '[con_mark=current]' focus >/dev/null
    i3-msg unmark current >/dev/null
    i3-msg workspace webcast >/dev/null
    i3-msg move workspace to output left >/dev/null
    i3-msg workspace webcast >/dev/null
       if [ -n "${USE_WEBCAM+x}" ] && ! i3-save-tree --workspace=webcast | grep -q '"floating": "user_on"'; then
           i3-msg exec i3-sensible-terminal >/dev/null
           sleep 1
           i3-msg floating enable >/dev/null
           i3-msg resize set $((WEBCAM_WIDTH-57)) $((WEBCAM_HEIGHT-50)) >/dev/null
           i3-msg move absolute position $((1920 +57 - OFFSET_X_RIGHT - WEBCAM_WIDTH)) $((1080 +41 - OFFSET_Y_BOTTOM - WEBCAM_HEIGHT)) >/dev/null
           i3-msg focus tiling >/dev/null || true
       fi
    # TODO some way to reverse this, for now, do it manually
}

[ -f video.mkv ] && {
    echo "video.mkv already exists. Remove it and rerun this command"
    exit 1
}
echo "You are about to record a video"
if [ -n "${USE_WEBCAM+x}" ]; then
    echo "You will be filming via the webcam also"
fi

if [ $# -eq 0 ] || [ "$1" != "serenity" ]; then
    setup_i3
fi

SLEEP=1
echo "Starting to in $SLEEP seconds"
sleep $SLEEP

FFMPEG_ARGS=""

FFMPEG_ARGS="${FFMPEG_ARGS} -video_size ${WIDTH}x${HEIGHT} -framerate ${FRAMERATE} -f x11grab -thread_queue_size 512 -i :0.0+${OFFSET_X},${OFFSET_Y}"
if [ -n "${USE_WEBCAM+x}" ]; then
    FFMPEG_ARGS="${FFMPEG_ARGS} -f v4l2 -video_size ${WEBCAM_WIDTH}x${WEBCAM_HEIGHT} -framerate 30 -thread_queue_size 512 -i /dev/video0"
    if [ $# -ne 0 ] && [ "$1" = "serenity" ]; then
        FFMPEG_ARGS="${FFMPEG_ARGS} -filter_complex overlay=main_w-overlay_w-2:main_h-overlay_h-28:format=yuv444"
    else
        FFMPEG_ARGS="${FFMPEG_ARGS} -filter_complex overlay=main_w-overlay_w:main_h-overlay_h:format=yuv444"
    fi
fi

# microphone
# did pulse loopback module source=mic sink=laptop speaker (on headphones-unplugged)
# then have a combine-sink module
# then set scummvm/etc default output to be the combined one
# then record *only* off the monitor of laptop speaker (which should have both mic, and game audio)
# headphones should only have game audio
# pacmd load-module module-combine-sink
# pactl load-module module-loopback source=29 sink=22
# FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -ac 2 -thread_queue_size 512 -i 1"
#FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -ac 2 -thread_queue_size 512 -i default"
#if [ -n "${MONITOR_MODE+x}" ]; then
#    # this is needed to listen to sounds played on computer
#    FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -thread_queue_size 512 -i ${PULSE_MONITOR_ID}"
#    # need amerge to merge in mic and monitor
#    # but having it makes it out of sync...
#    FFMPEG_ARGS="${FFMPEG_ARGS} -filter_complex amerge"
#fi

PS4=
if [ $# -ge 2 ] && [[ "$2" == "loopback" ]]; then
    # from https://stackoverflow.com/questions/61036703/recording-headset-output-mic-input-for-karaoke/61535859#61535859
    cleanup=$({
        pactl load-module module-null-sink sink_name=loopback_combine sink_properties="device.description='Output\ and\ Mic\ Combiner'"
        pactl load-module module-null-sink sink_name=loopback_split sink_properties="device.description='Output\ Splitter'"
        pactl load-module module-loopback source=loopback_split.monitor
        pactl load-module module-loopback source=loopback_split.monitor sink=loopback_combine
        pactl load-module module-loopback source=easyeffects_source sink=loopback_combine
    } | while read -r i; do
        echo "pactl unload-module $i"
    done | tr '\n' ';' | sed 's/;$//')
echo "cleanup is ${cleanup:+$cleanup}"
    FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -ac 2 -thread_queue_size 512 -i loopback_combine.monitor"
else
    FFMPEG_ARGS="${FFMPEG_ARGS} -f pulse -ac 2 -thread_queue_size 512 -i easyeffects_source"
fi

FFMPEG_ARGS="${FFMPEG_ARGS} -c:v libx264rgb -crf 0 -preset ultrafast"

# TODO make a tmux running on the screen at right place
# TODO ^^ should also change hostname to hide that
killall -SIGUSR1 dunst
cleanup="${cleanup:+$cleanup; }killall -SIGUSR2 dunst"
touch ~/recording
cleanup="${cleanup:+$cleanup; }rm ~/recording"
echo "Running $cleanup at finish"
trap "$cleanup" INT
trap "$cleanup" EXIT
set -x
# shellcheck disable=SC2086
ffmpeg ${FFMPEG_ARGS} video.mkv &
ff=$!
while sleep 10; do
    kill -0 $ff 2>/dev/null || break
    space=$(df --output=pcent . | awk 'NR==2 {print $1}' | tr -d '%')
    if [[ "$space" -lt 10 ]]; then
        echo "Not much space left"
        kill $ff 2>/dev/null
        wait $ff 2>/dev/null
        break
    fi
done
