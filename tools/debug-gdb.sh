#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

export DEBUG=1
"${TOOLS_DIR}"/build.sh
gdb -q -x "${TOOLS_DIR}"/debug.gdbinit
