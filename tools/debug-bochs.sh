#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

"${TOOLS_DIR}"/build.sh
# Needs 10m min
bochs -q -f "${TOOLS_DIR}"/debug.bochrc.txt
