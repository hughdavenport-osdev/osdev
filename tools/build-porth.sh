#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

git clone https://gitlab.com/tsoding/porth "${PORTH_DIR}"
cd "${PORTH_DIR}" || exit 1 # would only fail if git clone does
fasm -m 524288 ./bootstrap/porth-linux-x86_64.fasm
chmod +x ./bootstrap/porth-linux-x86_64
./bootstrap/porth-linux-x86_64 com ./porth.porth
