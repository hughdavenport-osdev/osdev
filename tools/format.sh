#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

SHFMT_ARGS="${SHFMT_ARGS:-"-w"}"
REMARK_ARGS="${REMARK_ARGS:-"--output --use remark-preset-lint-recommended"}"
CLANG_FORMAT_ARGS="${CLANG_FORMAT_ARGS:-"-style=webkit -Werror -i"}"
ASMFMT_ARGS="${ASMFMT_ARGS:-"-w"}"

# shellcheck disable=SC2086
shfmt ${SHFMT_ARGS} .
# shellcheck disable=SC2086
remark ${REMARK_ARGS} .
# shellcheck disable=SC2086
find . -name '*.c' -print0 | xargs -0 clang-format ${CLANG_FORMAT_ARGS}
# shellcheck disable=SC2086
find . -name '*.h' -print0 | xargs -0 clang-format ${CLANG_FORMAT_ARGS}

ASMFMT_OUT=$(mktemp)
# shellcheck disable=SC2086
find . -name '*.asm' -print0 | xargs -0 asmfmt ${ASMFMT_ARGS} >"${ASMFMT_OUT}"
# shellcheck disable=SC2086
find . -name '*.inc' -print0 | xargs -0 asmfmt ${ASMFMT_ARGS} >>"${ASMFMT_OUT}"
# asmfmt currently doesn't return an error when files are not formatted
if [ -s "${ASMFMT_OUT}" ]; then
    echo "asmfmt found errors"
    cat "${ASMFMT_OUT}"
    rm "${ASMFMT_OUT}"
    false
else
    rm "${ASMFMT_OUT}"
fi
