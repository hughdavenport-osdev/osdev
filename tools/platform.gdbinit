symbol-file -readnow build/osdev-platform.symbols

## Show some useful layouts then focus the cmd pane

layout asm
layout regs
focus cmd

set logging on

## Prefer intel vs at&t
set disassembly-flavor intel

source tools/gdb-functions.gs

break _start
run
