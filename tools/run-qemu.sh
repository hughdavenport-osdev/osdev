#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

"${TOOLS_DIR}"/build.sh
# TODO, use -readconfig, as well as in tools/debug.gdbinit
qemu-system-i386 -audiodev sdl,id=snd0 -machine pcspk-audiodev=snd0 -drive file="${BUILD_DIR}/${OS_NAME}.img",format=raw -m 3G
