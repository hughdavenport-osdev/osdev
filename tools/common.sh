# shellcheck shell=sh

set -eu # Fail on errors or bad variables
# shellcheck disable=SC2039 disable=SC3040
[ -n "${BASH+x}" ] && set -o pipefail

[ -n "${VERBOSE+x}" ] && set -x # show execution trace

run() {
    if [ -n "${VERBOSE+x}" ]; then
        "$@"
    else
        "$@" >/dev/null
    fi
}

export SRC_DIR
export TOOLS_DIR
export BUILD_DIR
export BASE_DIR
export OS_NAME

SRC_DIR="$(dirname "$0")"/../src
TOOLS_DIR="$(dirname "$0")"
BUILD_DIR="$(dirname "$0")"/../build
BASE_DIR="$(dirname "$0")"/..
OS_NAME="osdev"
PORTH_DIR="${PORTH_DIR:-${HOME}/src/third-party/porth}"
