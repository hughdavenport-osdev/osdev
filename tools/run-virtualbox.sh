#!/bin/bash
# FIXME, the INT trap below doesn't work in sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

"${TOOLS_DIR}"/build.sh
# Make a vbox image
if VBoxManage showvminfo "${OS_NAME}" >/dev/null 2>/dev/null; then
    if VBoxManage showvminfo osdev | grep "^State:" | grep "running" >/dev/null; then
        VBoxManage controlvm "${OS_NAME}" poweroff
        sleep 1
    fi
    VBoxManage unregistervm "${OS_NAME}" --delete
fi
rm "${BUILD_DIR}/${OS_NAME}.vdi" || true
VBoxManage convertdd "${BUILD_DIR}/${OS_NAME}.img" "${BUILD_DIR}/${OS_NAME}.vdi" --format VDI --variant Fixed
VBoxManage createvm --name "${OS_NAME}" --register
UUID=$(VBoxManage showvminfo osdev | grep ^UUID | sed 's/.*: *//')
VBoxManage storagectl "${OS_NAME}" --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach "${OS_NAME}" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "${BUILD_DIR}/${OS_NAME}.vdi"
VBoxManage startvm --type sdl "${OS_NAME}"
PID=$(pgrep -f "${UUID}")
trap 'echo "Killing '"${PID}"'"; kill -INT '"${PID}"'; kill -KILL '"${PID}" EXIT
tail --pid="$PID" -f /dev/null
