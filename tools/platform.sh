#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

UTILS=(shell hello)
usage() {
    echo "Usage: tools/platform.sh PROGNAME" >&2
    echo "" >&2
    echo "Available programs:" >&2
    for prog in "${UTILS[@]}"; do
        echo " - ${prog}" >&2
    done
}

if [ $# -lt 1 ]; then
    set +x
    usage
    exit 1
fi
PROGNAME="${1}"
valid=0
for prog in "${UTILS[@]}"; do
    if [ "${PROGNAME}" = "${prog}" ]; then
        valid=1
        break
    fi
done
if [ "$valid" = 0 ]; then
    set +x
    echo "Invalid program \"${PROGNAME}\"" >&2
    usage
    exit 1
fi
if [ -n "${2+x}" ]; then
    export DEBUG=1
fi

LIBS=(string stdio ctype stdlib unistd termios readkey ring_buffer libgen)
C_TESTS=(itoa atoi printf isdigit strcmp strlen puts strnrev putchar getchar fgets colour strtok malloc charmap)
export ASM_FILES="${LIBS[*]/#/lib\/} ${UTILS[*]/#/utils\/}"
export C_FILES="platform test ${C_TESTS[*]/#/tests\/}"
export KERNEL="none"

# shellcheck source=tools/build-source.sh
. "$(dirname "$0")"/build-source.sh

LDFLAGS="-melf_i386 -N"
WRAPPED_FUNCS=(sleep usleep)
# shellcheck disable=SC2048
for FUNC in ${WRAPPED_FUNCS[*]}; do
    LDFLAGS="$LDFLAGS --wrap=$FUNC"
done
# shellcheck disable=SC2086
run ld ${LDFLAGS} ${OBJS} -o "${BUILD_DIR}/${PROGNAME}" 2>/dev/null ||
    run ld ${LDFLAGS} ${OBJS} -o "${BUILD_DIR}/${PROGNAME}" # if it failes then show stderr
# shellcheck disable=SC2086
rm ${OBJS}
find "${BUILD_DIR}" -depth -type d -empty -delete
tty >/dev/null 2>&1 && {
    TTY_STATE=$(stty -g)
    stty -icanon -echo
    reset() {
        # shellcheck disable=SC2086
        stty $TTY_STATE
        [ -f "${BUILD_DIR}/${PROGNAME}" ] && rm "${BUILD_DIR}/${PROGNAME}"
    }
    trap reset INT
    trap reset TERM
    trap reset EXIT
}
# Turn off tracing if any
set +x
if [ -v DEBUG ]; then
    # First find the LOAD address of prog, then link the symbols using the debug objects
    ADDR=$(readelf -l "${BUILD_DIR}/${PROGNAME}" | grep LOAD | sed -e 's/  */ /g' -e 's/^ *//' | cut -d ' ' -f 3)
    if [ -z "$ADDR" ]; then
        echo "Could not find the start of the program to load debug symbols" >&2
        exit
    fi
    TMP_LINKER=$(mktemp)
    cat <<EOF >"${TMP_LINKER}"
SECTIONS {
    . = $ADDR;
}
EOF

    # shellcheck disable=SC2086
    ld -melf_i386 -T "${TMP_LINKER}" -T "${SRC_DIR}"/linker_symbols_platform.ld ${DEBUG_OBJS} -o "${BUILD_DIR}/${OS_NAME}-platform.symbols"
    rm "${TMP_LINKER}"
    # shellcheck disable=SC2086
    rm ${DEBUG_OBJS}
    gdb -q -x tools/platform.gdbinit "${BUILD_DIR}/${PROGNAME}"
    exit
fi
"${BUILD_DIR}/${PROGNAME}"
