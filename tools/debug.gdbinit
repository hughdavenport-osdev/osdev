## Start qemu and hook into it
# TODO, use -readconfig, as well as in tools/run.sh
target remote | exec qemu-system-i386 -audiodev sdl,id=snd0 -machine pcspk-audiodev=snd0 -drive file=build/osdev.img,format=raw -S -gdb stdio -m 3G
add-symbol-file build/osdev.symbols 0x7c00
python
def try_pass(arg):
    try:
        gdb.execute (arg)
    except:
        pass
end

define q
    # This fails if qemu closes before we quit
    python
try_pass ("monitor quit")
    end
    quit
end

## Show some useful layouts then focus the cmd pane

layout asm
layout regs
focus cmd

set logging on

## Prefer intel vs at&t
set disassembly-flavor intel

break int3_handler

# Debug 16 bit real mode, or 32 bit protected mode
if 0 == 1
    ## boot: (boot/boot32.inc) (16 bit real mode)
    set architecture i8086
    break *0x7c00
    define hook-stop
        set $debug_break = *(short*)0x7c09
        echo Setting breakpoint for debug_break:\n
        break *$debug_break
        # Reset hook-stop
        define hook-stop
        end
    end
end

source tools/gdb-functions.gs
