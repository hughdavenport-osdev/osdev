#!/bin/sh

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

SHELLCHECK_ARGS="-a --norc"

# shellcheck disable=SC2086
find . -name '*.sh' -print0 | xargs -0 shellcheck ${SHELLCHECK_ARGS}

SHFMT_ARGS="-d"
ASMFMT_ARGS="-d"
REMARK_ARGS="--frail --use remark-preset-lint-recommended"
CLANG_FORMAT_ARGS="-style=webkit -Werror --dry-run"
# shellcheck source=tools/format.sh
. "$(dirname "$0")"/format.sh

[ -f ./asmcheck ] || rustc tools/asmcheck.rs
find . -name '*.asm' -print0 | xargs -0 ./asmcheck

echo "All lint checks passed"
