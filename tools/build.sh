#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

# shellcheck source=tools/build-source.sh
. "$(dirname "$0")"/build-source.sh

LDFLAGS="-melf_i386"
# shellcheck disable=SC2086
ld ${LDFLAGS} -T "${SRC_DIR}"/linker.ld ${OBJS} -o "${BUILD_DIR}/${OS_NAME}.img"
# Make it atleast 10M and align to 512 bytes
DISK_SIZE="$(stat -c '%s' "${BUILD_DIR}/${OS_NAME}.img")"
if [ "${DISK_SIZE}" -lt 10321920 ]; then
    truncate -s 10321920 "${BUILD_DIR}/${OS_NAME}.img"
else
    truncate -s $(((DISK_SIZE + 511) / 512 * 512)) "${BUILD_DIR}/${OS_NAME}.img"
fi
if [ -v DEBUG ]; then
    # shellcheck disable=SC2086
    ld -melf_i386 -T "${SRC_DIR}"/linker_symbols.ld ${DEBUG_OBJS} -o "${BUILD_DIR}/${OS_NAME}.symbols"
    # shellcheck disable=SC2086
    rm ${DEBUG_OBJS}
fi
# shellcheck disable=SC2086
rm ${OBJS}
find "${BUILD_DIR}" -depth -type d -empty -delete
