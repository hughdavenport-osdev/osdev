#!/bin/bash

# shellcheck source=tools/common.sh
. "$(dirname "$0")"/common.sh

PORTH="${PORTH_DIR}/porth"
PORTHFLAGS="-I ${PORTH_DIR}/std"
KERNEL="${KERNEL:=asm}"
CC="cc"
CFLAGS="-m32 -nostdlib -ffreestanding -fno-pic -Wall -Wextra -Werror -Wno-unknown-warning-option -Wno-dangling-pointer"
OBJS=""
if [ -v DEBUG ]; then
    DEBUG_OBJS=""
fi
# FIXME: The array format and prefix replacement is only in bash, but it is nice...
DRIVERS=(vga_text pic8259 ps2-keyboard pit)
LIBS=(string stdio ctype stdlib unistd music termios pthread execinfo term readkey ring_buffer window winman libgen)
UTILS=(shell music term)
C_LIBS=()
TESTS=()
C_TESTS=(itoa atoi printf isdigit strcmp strlen puts strnrev putchar getchar fgets colour strtok malloc charmap)
ASM_FILES="${ASM_FILES:-boot/boot ${DRIVERS[*]/#/drivers\/} ${LIBS[*]/#/lib\/} ${UTILS[*]/#/utils\/} ${TESTS[*]/#/tests\/} scheduler memory}"
C_FILES="${C_FILES:-test ${C_LIBS[*]/#/lib\/} /${C_TESTS[*]/#/tests\/}}"
if [ "${KERNEL}" = "c" ]; then
    C_FILES="${C_FILES} kernel"
elif [ "${KERNEL}" != "none" ]; then
    ASM_FILES="${ASM_FILES} kernel"
fi
if [ -v DEBUG ]; then
    # shellcheck disable=SC2086
    run ${PORTH} ${PORTHFLAGS} com "${TOOLS_DIR}/generate_debug_symbols.porth"
    rm "${TOOLS_DIR}/generate_debug_symbols.asm" # Autogenerated
fi
for FILE in ${ASM_FILES}; do
    if [ ! -f "${SRC_DIR}/${FILE}.asm" ]; then continue; fi
    mkdir -p "$(dirname "${BUILD_DIR}/${FILE}")"
    if [ -v DEBUG ]; then
        run fasm "${SRC_DIR}/${FILE}.asm" -s "${BUILD_DIR}/${FILE}.dbg"
        run "${TOOLS_DIR}/generate_debug_symbols" "${BUILD_DIR}/${FILE}.dbg"
        run fasm "${SRC_DIR}/${FILE}_debug.asm"
        mv "${SRC_DIR}/${FILE}_debug.o" "${BUILD_DIR}/${FILE}_debug.o"
        rm "${BUILD_DIR}/${FILE}.dbg"
        rm "${SRC_DIR}/${FILE}_debug.asm"
        DEBUG_OBJS="$DEBUG_OBJS ${BUILD_DIR}/${FILE}_debug.o"
    else
        run fasm "${SRC_DIR}/${FILE}.asm"
    fi
    mv "${SRC_DIR}/${FILE}.o" "${BUILD_DIR}/${FILE}.o"
    OBJS="$OBJS ${BUILD_DIR}/${FILE}.o"
done
for FILE in ${C_FILES}; do
    if [ ! -f "${SRC_DIR}/${FILE}.c" ]; then continue; fi
    mkdir -p "$(dirname "${BUILD_DIR}/${FILE}")"
    # shellcheck disable=SC2086
    ${CC} ${CFLAGS} "${SRC_DIR}/${FILE}.c" -c -o "${BUILD_DIR}/${FILE}.o"
    OBJS="$OBJS ${BUILD_DIR}/${FILE}.o"
    if [ -v DEBUG ]; then
        cp "${BUILD_DIR}/${FILE}.o" "${BUILD_DIR}/${FILE}_debug.o"
        DEBUG_OBJS="$DEBUG_OBJS ${BUILD_DIR}/${FILE}_debug.o"
    fi
done
