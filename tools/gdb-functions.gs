define reload
    source tools/gdb-functions.gs
end

define print_stack
    x/i $eip
    set $base = $ebp
    x/x $base
    while $base != 0
        set $ret = *(void**)($base+4)
        x/i $ret
        set $base = *(void**)$base
        x/x $base
    end
end

define print_file_descriptor
    set $f=$arg0
    printf "fd struct at %p\n", $f
    printf "  object = %p\n", *(void**)($f+0)
    printf "  putc = %p -- ", *(void**)($f+4)
    info symbol *(void**)($f+4)
    printf "  getc = %p -- ", *(void**)($f+8)
    info symbol *(void**)($f+8)
    printf "  ungetc = %p -- ", *(void**)($f+12)
    info symbol *(void**)($f+12)
    printf "  termios = %p\n", *(void**)($f+16)
    printf "  readkey = %p\n", *(void**)($f+20)
end

define print_thread_info
    set $t=$arg0
    printf "thread struct at %p\n", $t
    printf "pid = %d\n", *(int*)$t
    printf "stack_top = %p\n", *(void**)($t+4)
    printf "esp = %p\n", *(void**)($t+8)
    printf "cr3 = %p\n", *(void**)($t+12)
    printf "next = %p\n", *(void**)($t+16)
    printf "state = %d\n", *(int*)($t+20)
    printf "fds_len = %d\n", *(int*)($t+24)
    printf "fds = %p\n", *(void**)($t+28)
    set $fd=0
    set $fd_len=*(int*)($t+24)
    set $fds=*(void**)($t+28)
    while $fd < $fd_len
        printf "fd %d:\n", $fd
        print_file_descriptor *(void**)($fds+4*$fd)
        set $fd=$fd+1
    end
end

define print_ring_buffer
    set $b=$arg0
    printf "len = %d, start = %d, end = %d\n", *(int*)$b, *(int*)($b+4), *(int*)($b+8)
    set $start=*(int*)($b+4)
    set $end=*(int*)($b+8)
    set $buffer=*(char**)($b+16)
    printf "buf = '%s", $buffer+$start
    if $end < $start
        printf "%s", $buffer
    end
    printf "'\n"
end

define print_current_thread_info
    print_thread_info *(void**)hack_current_task
end

define print_stdin
    print_file_descriptor *(void**)stdin
end

define print_stdout
    print_file_descriptor *(void**)stdout
end

define print_stderr
    print_file_descriptor *(void**)stderr
end
