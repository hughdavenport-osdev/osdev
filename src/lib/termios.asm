	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  tcgetattr
	public  tcsetattr
	public  termios_putc
	public  termios_getc
	public  termios_wait
	public  termios_new
	public  cfmakeraw

	include '../includes/kernel.inc'
	include '../includes/ascii.inc'
	include '../lib/includes/termios_constants.inc'
	include '../lib/includes/unistd.inc'
	include '../lib/includes/stdio.inc'
	include '../lib/includes/stdlib.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/ring_buffer.inc'

	fd equ arg1
	termios_p equ arg2
	;; int tcgetattr(int fd, struct termios *termios_p)

tcgetattr:
	enter 0, 0
	push  ebx

	;   TODO other fd's
	cmp dword fd, STDIN_FILENO
	jne .error

	mov eax, [stdin]
	mov ebx, [stdio_file.termios]
	mov edx, [termios_object.termios]

	mov   ecx, termios_p
	ccall memcpy, ecx, edx, sizeof.termios
	test  eax, eax
	jz    .error

.success:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	fd equ arg1
	optional_actions equ arg2
	termios_p equ arg3
	;; int tcsteattr(int fd, int optional_actions, const struct termios *termios_p)

tcsetattr:
	enter 0, 0
	push  ebx

	;   TODO other fd's
	cmp dword fd, STDIN_FILENO
	jne .error

	;   TODO other action modes
	cmp dword optional_actions, TCSANOW
	jne .error

	mov eax, [stdin]
	mov ebx, [stdio_file.termios]
	mov edx, [termios_object.termios]

	mov   ecx, termios_p
	ccall memcpy, edx, ecx, sizeof.termios
	test  eax, eax
	jz    .error

.success:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;;     void termios_wait(FILE *stream)
	stream equ arg1

termios_wait:
	enter 0, 0
	push  ebx
	push  esi

	mov  eax, stream
	mov  ebx, [stdio_file.termios]
	test ebx, ebx
	jz   .error
	mov  edx, [termios_object.termios]
	test edx, edx
	jz   .error

	mov eax, [termios.c_cc]
	add eax, VMIN
	mov esi, [eax]
	mov ebx, [termios_object.out_buffer]

	cmp esi, [ring_buffer.length]
	jle .return

	mov   eax, stream
	mov   ecx, [stdio_file.object]
	mov   edx, [stdio_file.getc]
	ccall edx, ecx
	cmp   eax, -1
	je    .wait
	ccall ring_buffer_putc, eax, ebx

.wait_loop:
	cmp esi, [ring_buffer.length]
	jle .return

.wait:
	;     TODO yield or something?
	;     ideally termios_putc will notify us when data is available
	extrn schedule
	ccall schedule
	mov   eax, stream
	mov   ecx, [stdio_file.object]
	mov   edx, [stdio_file.getc]
	ccall edx, ecx
	cmp   eax, -1
	je    .wait
	ccall ring_buffer_putc, eax, ebx
	jmp   .wait_loop

.error:
	mov eax, -1

.return:
	pop esi
	pop ebx
	leave
	ret

	;;     int termios_getc(FILE *stream)
	stream equ arg1

termios_getc:
	enter 0, 0
	push  ebx
	push  esi

	; first, pull from termios.out_buffer
	; if non avail, pull from stream.getc
	; TODO do any processing needed?

	mov  eax, stream
	mov  ebx, [stdio_file.termios]
	test ebx, ebx
	jz   .error
	mov  edx, [termios_object.termios]
	test edx, edx
	jz   .error

	mov   ebx, [termios_object.out_buffer]
	ccall ring_buffer_getc, ebx
	cmp   eax, -1
	jne   .return

	mov   eax, stream
	mov   ecx, [stdio_file.getc]
	mov   edx, [stdio_file.object]
	test  ecx, ecx
	jz    .error
	ccall ecx, edx

	jmp .return

.error:
	mov eax, -1

.return:
	pop esi
	pop ebx
	leave
	ret

	;;     int termios_putc(char c, FILE *stream)
	c      equ arg1
	stream equ arg2

termios_putc:
	enter 0, 0
	push  ebx

	mov eax, stream
	mov ecx, c
	mov ebx, [stdio_file.termios]
	mov edx, [termios_object.termios]

	test dword [termios.c_lflag], ICANON
	jz   .putc

	;   Canonical mode, buffer in raw buffer
	cmp cl, ASCII_BACKSPACE
	je  .backspace
	cmp cl, ASCII_NEWLINE
	je  .swap_bufs
	;   delete makes no sense with no ability for cursor to go back
	;   escape makes no sense in canon mode

	jmp .termios_putc

.backspace:

	mov   ecx, [termios_object.in_buffer]
	ccall ring_buffer_unputc, ecx
	jmp   .echo

.swap_bufs:
	; TODO, if the underlying stream uses a buf, then we can memcpy

	push esi
	push edi
	push ebx

	mov esi, [termios_object.in_buffer]
	mov eax, stream
	mov ebx, [stdio_file.putc]
	mov edi, [stdio_file.object]

	ccall ring_buffer_getc, esi

.loop:
	cmp   eax, -1
	je    .end_loop
	ccall ebx, eax, edi
	;     TODO notify termios_wait_for_min
	ccall ring_buffer_getc, esi
	jmp   .loop

.end_loop:
	pop ebx
	pop edi
	pop esi
	jmp .putc

.termios_putc:
	mov   edx, [termios_object.in_buffer]
	ccall ring_buffer_putc, ecx, edx
	jmp   .echo

.putc:
	mov   eax, stream
	mov   ecx, [stdio_file.putc]
	mov   edx, [stdio_file.object]
	mov   eax, c
	ccall ecx, eax, edx
	;     TODO notify termios_wait_for_min

	jmp .echo

.echo:
	mov eax, stream
	mov ebx, [stdio_file.termios]
	mov edx, [termios_object.termios]
	mov eax, c

	test dword [termios.c_lflag], ECHO
	jz   .return

	cmp al, ASCII_ESCAPE
	je  .echo_escape
	;   TODO vt or xterm escapes

	mov   ecx, [termios_object.echo_stream]
	ccall putc, eax, ecx

	jmp .return

.echo_escape:
	mov   eax, [termios_object.echo_stream]
	ccall fputs, .escape_str, eax
	mov   eax, ASCII_ESCAPE
	jmp   .return

.escape_str:
	db "^[", 0

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;; void termios_set_defaults(struct termios *t)
	t  equ arg1

termios_set_defaults:
	enter 0, 0

	mov edx, t

	mov [termios.c_lflag], ICANON or ECHO

	mov eax, [termios.c_cc]
	add eax, VMIN
	mov dword [eax], 1

	leave
	ret

	;; void cfmakeraw(struct termios *t)
	t  equ arg1

cfmakeraw:
	enter 0, 0

	mov edx, t

	mov eax, [termios.c_lflag]
	and eax, not ICANON and not ECHO
	mov [termios.c_lflag], eax

	leave
	ret

	;; struct termios_object *termios_new(FILE *echo_stream)
	echo_stream equ arg1

termios_new:
	enter 0, 0
	push  ebx

	ccall malloc, sizeof.termios
	test  eax, eax
	jz    .error
	mov   edx, eax
	push  edx
	ccall malloc, sizeof.termios_object
	pop   edx
	test  eax, eax
	jz    .error
	mov   ebx, eax

	push  edx
	ccall calloc, WORD_SIZE, LEN_CC
	pop   edx
	test  eax, eax
	jz    .error
	mov   [termios.c_cc], eax

	mov   [termios_object.termios], edx
	ccall termios_set_defaults, edx

	mov ecx, echo_stream
	mov [termios_object.echo_stream], ecx

	ccall ring_buffer_new
	mov   [termios_object.in_buffer], eax

	ccall ring_buffer_new
	mov   [termios_object.out_buffer], eax

	mov eax, ebx

.error:
	pop ebx
	leave
	ret
