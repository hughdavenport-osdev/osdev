	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  ring_buffer_new
	public  ring_buffer_putc
	public  ring_buffer_getc
	public  ring_buffer_unputc
	public  ring_buffer_ungetc

	include 'includes/ring_buffer_struc.inc'
	include 'includes/stdlib.inc'
	include '../includes/kernel.inc'

	RING_BUFFER_INITIAL_CAPACITY = 4096

	;; struct ring_buffer ring_buffer_new()

ring_buffer_new:
	enter 0, 0
	push  ebx

	ccall malloc, sizeof.ring_buffer
	test  eax, eax
	jz    .error
	mov   ebx, eax
	ccall malloc, RING_BUFFER_INITIAL_CAPACITY
	test  eax, eax
	jz    .error
	mov   [ring_buffer.length], 0
	mov   [ring_buffer.start], 0
	mov   [ring_buffer.end], 0
	mov   [ring_buffer.capacity], RING_BUFFER_INITIAL_CAPACITY
	mov   [ring_buffer.buffer], eax

	mov eax, ebx
	jmp .return

.error:
	mov eax, NULL

.return:
	pop ebx
	leave
	ret

	;;  int ring_buffer_putc(int c, struct ring_buffer buf)
	c   equ arg1
	buf equ arg2

ring_buffer_putc:
	enter 0, 0
	push  ebx

	mov  ebx, buf
	test ebx, ebx
	jz   .error

	mov ecx, [ring_buffer.length]
	cmp ecx, [ring_buffer.capacity]
	jge .error

	mov ecx, [ring_buffer.buffer]
	add ecx, [ring_buffer.end]
	mov eax, c
	mov [ecx], al

	mov eax, [ring_buffer.end]
	inc eax
	xor edx, edx
	div [ring_buffer.capacity]
	mov [ring_buffer.end], edx

	inc [ring_buffer.length]

	mov eax, c
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;;  int ring_buffer_getc(struct ring_buffer buf)
	buf equ arg1

ring_buffer_getc:
	enter 0, 0
	push  ebx

	mov  ebx, buf
	test ebx, ebx
	jz   .error

	cmp [ring_buffer.length], 0
	je  .error

	xor eax, eax

	mov ecx, [ring_buffer.buffer]
	add ecx, [ring_buffer.start]
	mov al, [ecx]

	mov ecx, eax

	mov eax, [ring_buffer.start]
	inc eax
	xor edx, edx
	div [ring_buffer.capacity]
	mov [ring_buffer.start], edx

	dec [ring_buffer.length]

	mov eax, ecx

	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;;  int ring_buffer_ungetc(int c, struct ring_buffer buf)
	c   equ arg1
	buf equ arg2

ring_buffer_ungetc:
	enter 0, 0
	push  ebx

	mov  ebx, buf
	test ebx, ebx
	jz   .error

	mov ecx, [ring_buffer.length]
	cmp ecx, [ring_buffer.capacity]
	jge .error

	mov   eax, [ring_buffer.start]
	test  eax, eax
	cmovz eax, [ring_buffer.capacity]
	dec   eax
	mov   [ring_buffer.start], eax

	mov ecx, [ring_buffer.buffer]
	add ecx, eax
	mov eax, c
	mov [ecx], al

	inc [ring_buffer.length]

	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret

	;;  int ring_buffer_unputc(struct ring_buffer buf)
	buf equ arg1

ring_buffer_unputc:
	enter 0, 0
	push  ebx

	mov  ebx, buf
	test ebx, ebx
	jz   .error

	cmp [ring_buffer.length], 0
	je  .error

	mov   eax, [ring_buffer.end]
	test  eax, eax
	cmovz eax, [ring_buffer.capacity]
	dec   eax
	mov   [ring_buffer.end], eax

	mov ecx, [ring_buffer.buffer]
	add ecx, eax
	xor eax, eax
	mov al, [ecx]

	dec [ring_buffer.length]

	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret
