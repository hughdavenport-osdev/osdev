	format  elf
	section '.text' executable
	;;      32 bit protected mode
	use32

	public debug_add_function
	public debug_get_symbol

	include '../includes/public.inc'
	include '../includes/kernel.inc'
	include '../lib/includes/stdio.inc'

	public backtrace
	public backtrace_symbols

	DEBUG_CAP = 1024; number of functions allowed

	func equ arg1
	str  equ arg2

debug_add_function:
	enter 0, 0
	push  ebx
	push  esi

	mov    ebx, func
	mov    esi, str
	;ccall printf, .str, ebx, esi, esi

	cmp  dword [debug_symbols], DEBUG_CAP
	jge  .overflow
	mov  edx, [debug_symbols]
	;    this could be a shl edx, 3; knowing WORD_SIZE is 4
	imul edx, WORD_SIZE*2
	add  edx, debug_symbols+WORD_SIZE
	inc  dword [debug_symbols]
	sub  edx, 2*WORD_SIZE

.loop:
	cmp edx, debug_symbols+WORD_SIZE
	jl  .store
	mov eax, [edx]
	cmp eax, ebx
	jle .store
	mov ecx, [edx+WORD_SIZE]
	mov [edx+2*WORD_SIZE], eax
	mov [edx+3*WORD_SIZE], ecx
	sub edx, 2*WORD_SIZE
	jmp .loop

.store:
	mov [edx+2*WORD_SIZE], ebx
	mov [edx+3*WORD_SIZE], esi

	sub edx, debug_symbols-WORD_SIZE
	shr edx, 3

	; ccall printf, .store_str, edx

	; ccall debug_dump_functions

	jmp .return

.overflow:
	;      ccall  puts, .overflow_str
	;extrn panic
	;ccall panic

.return:
	pop esi
	pop ebx
	leave
	ret

.str:
	db "debug_add_function(func=%p, str=%s (%p))", 13, 10, 0

.store_str:
	db "stored at idx %d", 13, 10, 0

.overflow_str:
	db "OVERFLOW", 13, 10, 0

debug_dump_functions:
	enter 0, 0
	push  ebx
	push  esi

	mov   eax, [debug_symbols]
	ccall printf, .len_str, eax
	xor   ebx, ebx
	mov   esi, debug_symbols+WORD_SIZE

.loop:
	cmp   ebx, [debug_symbols]
	jge   .return
	test  ebx, ebx
	jz    .print
	ccall putchar, ','
	ccall putchar, ' '

.print:
	mov   eax, [esi]
	mov   ecx, [esi+WORD_SIZE]
	ccall printf, .fun_str, ecx, eax
	inc   ebx
	add   esi, WORD_SIZE*2
	jmp   .loop

.return:
	ccall putnl

	pop esi
	pop ebx
	leave
	ret

.len_str:
	db "debug_dump_functions: [%d]: ", 0

.fun_str:
	db "%s (%p)", 0

	;; char *debug_get_symbol(void *addr)

	addr equ arg1

debug_get_symbol:
	enter 0, 0

	xor  eax, eax
	;    TODO do a binary search to find the addr before eip
	;    for now, just do sequential
	mov  ecx, addr
	cmp  dword [debug_symbols], 0
	jle  .error
	mov  edx, [debug_symbols]
	imul edx, WORD_SIZE*2
	add  edx, debug_symbols+WORD_SIZE
	sub  edx, 2*WORD_SIZE

.loop:
	cmp edx, debug_symbols+WORD_SIZE
	jl  .return
	mov eax, [edx+WORD_SIZE]
	cmp ecx, [edx]
	je  .return
	jg  .add_offset
	sub edx, 2*WORD_SIZE
	jmp .loop

.add_offset:
	;   TODO add hex offset
	sub ecx, [edx]
	;   TODO use asprintf to alloc a new string with func+0xoffset
	;   asprintf should determine how much space is needed, then malloc that, then do snprintf
	;   could also just strlen(func), then add 33, malloc that, then strcpy, then call itoa with that+strlen

	push  eax
	ccall printf, .offset_str, eax, ecx
	ccall putnl

	;   ccall asprintf, .ptr_to_str, .offset_str, eax, ecx
	mov eax, [.ptr_to_str]
	pop eax

	jmp .return

.offset_str:
	db "%s+%p", 0

.ptr_to_str:
	dd 0

.error:
	xor eax, eax
	;   fall through to return

.return:
	leave
	ret

	;; int backtrace(void **buffer, int size)

	buffer equ arg1
	size   equ arg2

backtrace:
	enter 0, 0
	push  ebx
	push  esi

	mov esi, [ebp+4]
	mov ebx, [ebp]

	; ccall debug_dump_functions

.loop:
	;     FIXME: this shouldn't get the symbol, thats for backtrace_symbols to do
	;     TODO: this should store addr's in buffer, and stop on size
	extrn kernel_stack_top
	ccall debug_get_symbol, esi
	ccall printf, .printf_str, esi, eax
	test  ebx, ebx
	jz    .return
	cmp   ebx, kernel_stack_top
	jge   .return
	mov   esi, [ebx+4]
	mov   ebx, [ebx]
	jmp   .loop

.return:
	pop esi
	pop ebx
	leave
	ret

.printf_str:
	db "debug_get_symbol(%p) = %s", 13, 10, 0

	;; char **backtrace_symbols(void *const *buffer, int size)

	buffer equ arg1
	size   equ arg2

backtrace_symbols:
	enter 0, 0

.return:
	leave
	ret

debug_symbols:
	;     TODO could just use malloc?
	dd    0; length
	times DEBUG_CAP dd 0; addr
	times DEBUG_CAP dd 0; str
