	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  music_parse_note
	public  music_play_note
	public  music_play_scale
	public  music_play_chord

	extrn pit_play_sound
	extrn pit_no_sound

	include '../lib/includes/music_constants.inc'
	include '../lib/includes/string.inc'
	include '../lib/includes/stdio.inc'
	include '../lib/includes/unistd.inc'
	include '../includes/kernel.inc'
	include '../includes/ascii.inc'

note_frequencies:
	;  FIXME, we lose precision converting to int
	;  frequencies 16.35160 17.32391 18.35405 19.44544 20.60172 21.82676 23.12465 24.49971 25.95654 27.50000 29.13524 30.86771 32.70320 34.64783 36.70810 38.89087 41.20344 43.65353 46.24930 48.99943 51.91309 55.00000 58.27047 61.73541 65.40639 69.29566 73.41619 77.78175 82.40689 87.30706 92.49861 97.99886 103.8262 110.0000 116.5409 123.4708 130.8128 138.5913 146.8324 155.5635 164.8138 174.6141 184.9972 195.9977 207.6523 220.0000 233.0819 246.9417 261.6256 277.1826 293.6648 311.1270 329.6276 349.2282 369.9944 391.9954 415.3047 440.0000 466.1638 493.8833 523.2511 554.3653 587.3295 622.2540 659.2551 698.4565 739.9888 783.9909 830.6094 880.0000 932.3275 987.7666 1046.502 1108.731 1174.659 1244.508 1318.510 1396.913 1479.978 1567.982 1661.219 1760.000 1864.655 1975.533 2093.005 2217.461 2349.318 2489.016 2637.020 2793.826 2959.955 3135.963 3322.438 3520.000 3729.310 3951.066 4186.009 4434.922 4698.636 4978.032 5274.041 5587.652 5919.911 6271.927 6644.875 7040.000 7458.620 7902.133
	dd 16, 17, 18, 19, 20, 21, 23, 24, 25, 27, 29, 30
	dd 32, 34, 36, 38, 41, 43, 46, 48, 51, 55, 58, 61
	dd 65, 69, 73, 77, 82, 87, 92, 97, 103, 110, 116, 123
	dd 130, 138, 146, 155, 164, 174, 184, 195, 207, 220, 233, 246
	dd 261, 277, 293, 311, 329, 349, 369, 391, 415, 440, 466, 493
	dd 523, 554, 587, 622, 659, 698, 739, 783, 830, 880, 932, 987
	dd 1046, 1108, 1174, 1244, 1318, 1396, 1479, 1567, 1661, 1760, 1864, 1975
	dd 2093, 2217, 2349, 2489, 2637, 2793, 2959, 3135, 3322, 3520, 3729, 3951
	dd 4186, 4434, 4698, 4978, 5274, 5587, 5919, 6271, 6644, 7040, 7458, 7902

	note equ arg1

music_note_frequency:
	enter 0, 0

	mov eax, note
	shl eax, 2; * WORD_SIZE
	add eax, note_frequencies
	mov eax, [eax]

	leave
	ret

note     equ arg1
duration equ arg2

music_play_note:
	enter 0, 0

	mov   eax, note
	ccall music_note_frequency, eax
	mov   ecx, eax

	;     push  ecx
	;     ccall printf, .frequency_str, ecx
	;     pop   ecx
	ccall pit_play_sound, ecx

	mov   eax, duration
	ccall usleep, eax
	ccall pit_no_sound

	leave
	ret

.frequency_str:
	db "music: playing frequency %d", 13, 10, 0

	note     equ arg1
	chord    equ arg2
	duration equ arg3

music_play_chord:
	enter 0, 0
	push  ebx

	mov ebx, note

	mov eax, chord
	cmp eax, MUSIC_CHORD_MAJOR
	je  .major

	; TODO error, unknown chord
	; for now, it is a programmer error or mem corruption, silently ignore

	jmp .return

.major:
	;     can't play multiple frequencies at once?
	ccall music_note_frequency, ebx
	ccall pit_play_sound, eax
	mov   eax, 10
	ccall usleep, eax

	add   ebx, 4
	ccall music_note_frequency, ebx
	ccall pit_play_sound, eax
	mov   eax, 10
	ccall usleep, eax

	add   ebx, 3
	ccall music_note_frequency, ebx
	ccall pit_play_sound, eax
	mov   eax, 10
	ccall usleep, eax

	add   ebx, 5
	ccall music_note_frequency, ebx
	ccall pit_play_sound, eax
	mov   eax, 10
	ccall usleep, eax

	mov   eax, duration
	ccall sleep, eax
	ccall pit_no_sound

	jmp .return

.return:
	pop ebx
	leave
	ret

note  equ arg1
scale equ arg2

music_play_scale:
	enter 0, 0
	push  ebx

	mov ebx, note

	mov eax, scale
	cmp eax, MUSIC_SCALE_MAJOR
	je  .major
	cmp eax, MUSIC_SCALE_MINOR
	je  .minor
	cmp eax, MUSIC_SCALE_PENTATONIC_MAJOR
	je  .pentatonic_major
	cmp eax, MUSIC_SCALE_PENTATONIC_MINOR
	je  .pentatonic_minor

	; TODO error, unknown scale
	; for now, it is a programmer error or mem corruption, silently ignore

	jmp .return

.major:
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	inc   ebx
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	inc   ebx
	ccall music_play_note, ebx, 500

	jmp .return

.minor:
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	inc   ebx
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	inc   ebx
	ccall music_play_note, ebx, 500
	add   ebx, 3
	ccall music_play_note, ebx, 500
	inc   ebx
	ccall music_play_note, ebx, 500

	jmp .return

	; TODO different minors when doing descending as well

.pentatonic_major:

	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 3
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 3
	ccall music_play_note, ebx, 500

	jmp .return

.pentatonic_minor:

	ccall music_play_note, ebx, 500
	add   ebx, 3
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500
	add   ebx, 3
	ccall music_play_note, ebx, 500
	add   ebx, 2
	ccall music_play_note, ebx, 500

	jmp .return

.return:
	pop ebx
	leave
	ret

note_str equ arg1

music_parse_note:
	enter 0, 0
	push  esi
	push  ebx

	xor eax, eax
	xor ebx, ebx

	mov  esi, note_str
	test esi, esi
	jz   .error

	lodsb
	test al, al
	jz   .error

	cmp al, 'A'
	jl  .error
	cmp al, 'G'
	jg  .error

	sub al, 'A'
	add al, 5; notation starts at C0
	mov cl, 7
	div cl

	mov bl, ah

	;   make room for accidentals
	cmp bl, 1; c#
	cmc ; complement carry flag
	adc bl, 0

	cmp bl, 3; d#
	cmc ; complement carry flag
	adc bl, 0

	cmp bl, 6; f#
	cmc ; complement carry flag
	adc bl, 0

	cmp bl, 8; g#
	cmc ; complement carry flag
	adc bl, 0

	cmp bl, 10; a#
	cmc ; complement carry flag
	adc bl, 0

	cmp bl, 12; unreachable
	jge .error

	lodsb
	test al, al
	jz   .error

	cmp   al, '#'
	je    .sharp
	cmp   al, 'b'
	je    .flat
	cmove ebx, ecx
	jmp   .parse_octave

.sharp:
	inc bl
	jmp .read_octave

.flat:
	dec bl
	jmp .read_octave

.read_octave:
	lodsb
	test al, al
	jz   .error

.parse_octave:
	cmp al, '0'
	jl  .error
	cmp al, '8'
	jg  .error

	sub al, '0'
	mov cl, 12
	mul cl

	add bx, ax

	mov eax, ebx

	jmp .return

.error:
	mov eax, -1
	jmp .return

.return:
	pop ebx
	pop esi
	leave
	ret
