	;       TODO some better structures like AVL or red/black tree
	;       for now, doubly linked list
	struc   malloc_chunk {
	.used   db ?
	.length dd ?
	.next   dd ?
	.prev   dd ?
	}
	;       TODO make a struct/ends macro to hide the need for virtual
	;       see "https://stackoverflow.com/questions/41843715/defining-a-structure-in-fasm-which-of-the-2-ways-is-better-in-what-situation#48058223"
	virtual at eax
	malloc_chunk malloc_chunk
	sizeof.malloc_chunk = $ - malloc_chunk
	end     virtual
