include 'termios_constants.inc'

extrn tcgetattr
extrn tcsetattr
extrn termios_putc
extrn termios_getc
extrn termios_wait
extrn termios_new
extrn cfmakeraw
