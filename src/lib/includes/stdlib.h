extern char* itoa(int value, char* str, int base);
extern int atoi(char* str);
#include "stddef.h"
extern void* malloc(size_t size);
extern void* realloc(void* ptr, size_t size);
extern void* calloc(size_t nmemb, size_t size);
extern void free(void* ptr);
