	;;    char *strnrev(char *str, int n)
	extrn strnrev
	;;    char *strncpy(char *dest, const char *src, size_t n)
	extrn strncpy
	;;    void *memcpy(void *dest, void *src, size_t n)
	extrn memcpy
	;;    void *memset(void *s, int c, size_t n)
	extrn memset
	;;    size_t strlen(const char *s)
	extrn strlen
	;;    int strcmp(const char *s1, const char *s2)
	extrn strcmp
	;;    char *strtok(char *str, const char *delim)
	extrn strtok
	;;    char *strdup(const char *s)
	extrn strdup
	;;    char *strndup(const char *s, size_t n)
	extrn strndup
