struct ring_buffer {
    int length;
    int start;
    int end;
    int capacity;
    void* buffer;
};

extern struct ring_buffer* ring_buffer_new();
extern int ring_buffer_putc(int c, struct ring_buffer* buf);
extern int ring_buffer_getc(struct ring_buffer* buf);
extern int ring_buffer_unputc(struct ring_buffer* buf);
extern int ring_buffer_ungetc(int c, struct ring_buffer* buf);
