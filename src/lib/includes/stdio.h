struct __FILE;
typedef struct __FILE FILE;

extern void* stdin;
#define stdin (FILE*)stdin
extern void* stdout;
#define stdout (FILE*)stdout
extern void* stderr;
#define stderr (FILE*)stderr
// FIXME: When stdin/out/err is defined as extern, they are all just NULL...
// extern FILE* stdin;
// extern FILE* stdout;
// extern FILE* stderr;

extern int printf(const char* fmt, ...);
extern int putchar(int c);
extern int puts(const char* s);
extern int fputs(const char* s, FILE* stream);
extern void putnl();
extern int getchar();
extern int fgetc(FILE* stream);
extern int getc(FILE* stream);
extern char* fgets(char* s, int size, FILE* stream);

struct __FILE {
    // TODO, change this from a ring buffer
    void* object;
    void* putc;
    void* getc;
    void* ungetc;
    void* termios;
    void* readkey;
};
