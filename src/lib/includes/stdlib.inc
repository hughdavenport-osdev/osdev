	;;    char *itoa(int value, char *str, int base)
	;;    itoa is signed for base 10, and unsigned for other bases
	;;    utoa is unsigned
	;;    itoA uses upper case alphabet
	extrn itoa
	extrn utoa
	extrn itoA
	;;    int atoi(char *str)
	extrn atoi
	;;    void *malloc(size_t size)
	extrn malloc
	;;    void *calloc(size_t nmemb, size_t size)
	extrn calloc
	;;    void *realloc(void *ptr, size_t size)
	extrn realloc
	;;    void free(void* ptr)
	extrn free
