#define ICANON 0x1
#define ECHO 0x2

// optional_actions for tcsetattr
#define TCSANOW 0
#define TCSADRAIN 1
#define TCSAFLUSH 2

typedef unsigned int tcflag_t;
struct termios {
    tcflag_t c_iflag;
    tcflag_t c_oflag;
    tcflag_t c_cflag;
    tcflag_t c_lflag;
    tcflag_t* c_cc;
    // TODO c_cc arrag
};

extern int tcgetattr(int fd, struct termios* termios_p);
extern int tcsetattr(int fd, int optional_actions, const struct termios* termios_p);
extern struct termios* termios_new(FILE* echo_stream);
// TODO termious_update_inbuf?
