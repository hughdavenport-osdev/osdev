extern char* strnrev(char* str, int n);
#include <stddef.h>
// for size_t
extern char* strncpy(char* dest, const char* src, size_t n);
extern void* memcpy(void* dest, void* src, size_t n);
extern void* memset(void* s, int c, size_t n);
extern size_t strlen(const char* s);
extern int strcmp(const char* s1, const char* s2);
extern char* strtok(char* str, const char* delim);
extern char* strdup(const char* s);
extern char* strndup(const char* s, size_t n);
