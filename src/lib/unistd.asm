	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  sleep
	public  usleep
	public  write
	public  read
	public  fork
	public  close
	public  dup2
	public  pipe

	STDIN_FILENO equ 0
	STDOUT_FILENO equ 1
	STDERR_FILENO equ 2

	extrn pit_ms
	extrn pit_fraction

	include '../includes/ascii.inc'
	include '../includes/kernel.inc'
	include '../includes/scheduler.inc'
	include 'includes/stdio.inc'
	include 'includes/stdlib.inc'
	include 'includes/string.inc'
	include 'includes/termios.inc'
	include 'includes/ring_buffer.inc'

	;;      unsigned int sleep(unsigned int seconds)
	seconds equ arg1

sleep:
	enter 0, 0

	mov eax, seconds
	mov ecx, 1000
	mul ecx

	;     todo, we lose precision in edx, if seconds is more than 49 days...
	ccall usleep, eax

	leave
	ret

	;;   int usleep(useconds_t usec)
	usec equ arg1

usleep:
	enter 0, 0
	push  ebx
	push  esi

	cli
	mov ebx, [pit_ms]
	mov esi, [pit_ms+WORD_SIZE]
	sti
	;   todo should we care about pit_fractions for usleep, may need it in nanosleep
	;   usleep would probably call nanosleep then

	add ebx, usec
	adc esi, 0

.loop:
	;   todo, some concurrency locks needed here if also writing from pit_handler?
	;   for now, cli, but then we lose ticks?
	cli
	mov eax, [pit_ms+WORD_SIZE]
	cmp eax, esi
	jl  .wait
	jg  .success
	;   lower higher 64 bits is eq
	mov eax, [pit_ms]
	cmp eax, ebx
	jge .success

.wait:
	;   todo, some concurrency locks needed here if also writing from pit_handler?
	;   for now, cli, but then we lose ticks?
	sti
	nop
	nop
	nop
	nop
	nop
	nop
	jmp .loop

.success:
	sti
	;   todo should return number of seconds left if interrupted by a signal
	;   for now, return 0, as no concept of signal handlers
	mov eax, 0
	;   fall through

.return:
	pop esi
	pop ebx
	leave
	ret

	;;    ssize_t write(int fd, const void *buf, size_t count)
	fd    equ arg1
	buf   equ arg2
	count equ arg3

write:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	mov   eax, fd
	ccall filestream, eax
	mov   esi, buf
	mov   edi, [stdio_file.object]
	mov   ecx, [stdio_file.putc]

	mov  ebx, [stdio_file.termios]
	test ebx, ebx
	je   .write

	mov edi, eax
	mov ecx, termios_putc

.write:
	mov  ebx, count
	test ecx, ecx
	jz   .return

.loop:
	test ebx, ebx
	jz   .return
	xor  eax, eax
	lodsb

	push  ecx
	ccall ecx, eax, edi
	pop   ecx
	cmp   eax, -1
	je    .return

	dec ebx
	jmp .loop

.return:
	;     TODO notify the fd to say there is data there
	cmp   ecx, termios_putc
	jne   ._return
	extrn schedule
	ccall schedule

._return:
	mov eax, count
	sub eax, ebx
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;;    ssize_t read(int fd, const void *buf, size_t count)
	fd    equ arg1
	buf   equ arg2
	count equ arg3

read:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	mov   eax, fd
	ccall filestream, eax
	mov   edi, buf
	mov   esi, [stdio_file.object]
	mov   ecx, [stdio_file.getc]

	mov  ebx, [stdio_file.termios]
	test ebx, ebx
	je   .read

	push  eax
	ccall termios_wait, eax
	pop   eax
	mov   esi, eax
	mov   ecx, termios_getc

.read:
	mov  ebx, count
	test ecx, ecx
	jz   .return

.loop:
	test ebx, ebx
	jz   .return

	push  ecx
	ccall ecx, esi
	pop   ecx
	cmp   eax, -1
	je    .return

	stosb
	dec ebx

	;   FIXME should only do this if termios has CANON set?
	cmp eax, ASCII_NEWLINE
	je  .return

	jmp .loop

.return:
	mov eax, count
	sub eax, ebx
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;; pid_t fork()

fork:
	enter 0, 0
	push  ebx
	push  esi
	push  edi

	ccall new_task, .child
	test  eax, eax
	jz    .error

	;     parent
	mov   edi, eax
	ccall copy_stack_frame, edi, .child
	ccall start_task, edi
	mov   eax, [thread_control_block.pid]
	jmp   .return

.child:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

	;; void copy_stack_frame(struct thread_control_block *t, void *entry_point)
	t  equ arg1
	entry_point equ arg2

copy_stack_frame:
	enter 0, 0
	push  edi
	push  esi
	push  ebx

	mov esi, ebp

	mov edi, [hack_current_task]
	mov ebx, [thread_control_block.stack_top]
	sub ebx, esi

	mov edi, t
	mov edi, [thread_control_block.stack_top]
	sub edi, ebx

.loop:
	mov  ecx, [esi]
	test ecx, ecx
	jz   .end_loop
	sub  ecx, esi
	sub  ecx, WORD_SIZE
	add  esi, WORD_SIZE
	sub  ebx, WORD_SIZE
	cmp  ebx, 0
	jl   .end_loop

	mov eax, edi
	add eax, ecx
	add eax, WORD_SIZE
	mov [edi], eax
	add edi, WORD_SIZE
	sub ebx, ecx

	cmp ebx, 0
	jl  .end_loop

	push  ecx
	ccall memcpy, edi, esi, ecx
	pop   ecx

	add edi, ecx

	sub esi, WORD_SIZE
	mov esi, [esi]

	jmp .loop

.end_loop:
	mov dword [edi], NULL

	mov esi, ebp

	mov edi, [hack_current_task]
	mov ebx, [thread_control_block.stack_top]
	sub ebx, esi

	mov edi, t
	mov eax, [thread_control_block.stack_top]
	sub eax, ebx

	mov ebx, [eax]; this is the ebp pointer
	add eax, 2*WORD_SIZE; remove the enter and the call
	add eax, 2*WORD_SIZE; remove the two args, t and entry_point

	mov edx, entry_point
	sub eax, WORD_SIZE
	mov [eax], edx; ret
	sub eax, WORD_SIZE
	mov [eax], ebx; leave
	mov ebx, eax
	sub eax, WORD_SIZE
	mov [eax], ebx; ebp popped in switch_to_task

	ccall task_setup_switch_to_task_stack, eax

	mov [thread_control_block.esp], eax

	pop ebx
	pop esi
	pop edi
	leave
	ret

	;; int close(int fd)
	fd equ arg1

close:
	enter 0, 0
	push  edi

	mov eax, fd
	cmp eax, 0
	jl  .error

	;     FIXME
	extrn hack_current_task
	mov   edi, [hack_current_task]
	mov   ecx, [thread_control_block.file_descriptors_len]
	cmp   eax, ecx
	jge   .error

	; TODO call a close function on the stream

	mov   ecx, [thread_control_block.file_descriptors]
	imul  eax, WORD_SIZE
	add   ecx, eax
	cmp   dword [ecx], NULL
	je    .success
	push  ecx
	ccall free, [ecx]
	pop   ecx
	mov   dword [ecx], NULL

	; TODO reduce size of fds array

.success:
	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop edi
	leave
	ret

	;;    int dup2(int oldfd, int newfd)
	oldfd equ arg1
	newfd equ arg2

dup2:
	enter 0, 0
	push  esi
	push  edi
	push  ebx

	mov ebx, newfd
	cmp ebx, 0
	jl  .error
	mov esi, oldfd
	cmp esi, 0
	jl  .error
	cmp ebx, esi
	je  .return

	mov edi, [hack_current_task]
	mov ecx, [thread_control_block.file_descriptors_len]
	cmp ebx, ecx
	jge .error
	cmp esi, ecx
	jge .error

	ccall malloc, sizeof.stdio_file
	test  eax, eax
	jz    .error
	xchg  ebx, eax
	imul  eax, WORD_SIZE
	mov   ecx, [thread_control_block.file_descriptors]
	add   eax, ecx
	mov   [eax], ebx

	imul  esi, WORD_SIZE
	add   esi, ecx
	ccall memcpy, ebx, [esi], sizeof.stdio_file
	test  eax, eax
	jz    .error

	mov eax, newfd
	cmp eax, STDIN_FILENO
	je  .stdin
	cmp eax, STDOUT_FILENO
	je  .stdout
	cmp eax, STDERR_FILENO
	je  .stderr

	jmp .return

.stdin:
	mov [stdin], ebx
	jmp .return

.stdout:
	mov [stdout], ebx
	jmp .return

.stderr:
	mov [stderr], ebx
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	pop edi
	pop esi
	leave
	ret

	;;  int pipe(int[2] fds)
	fds equ arg1

pipe:
	enter 0, 0
	push  ebx

	ccall ring_buffer_new
	test  eax, eax
	jz    .error
	mov   ebx, eax

	ccall calloc, 1, sizeof.stdio_file
	test  eax, eax
	jz    .error

	mov [stdio_file.object], ebx
	mov [stdio_file.getc], ring_buffer_getc
	mov [stdio_file.ungetc], ring_buffer_ungetc

	;     FIXME this is a bit hacky
	extrn task_add_file
	ccall task_add_file, eax
	mov   ecx, fds
	mov   [ecx], eax

	ccall calloc, 1, sizeof.stdio_file
	test  eax, eax
	jz    .error

	mov [stdio_file.object], ebx
	mov [stdio_file.putc], ring_buffer_putc

	;     FIXME this is a bit hacky
	ccall task_add_file, eax
	mov   ecx, fds
	add   ecx, WORD_SIZE
	mov   [ecx], eax

	xor eax, eax
	jmp .return

.error:
	mov eax, -1

.return:
	pop ebx
	leave
	ret
