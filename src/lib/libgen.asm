	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  dirname
	public  basename

	include '../includes/kernel.inc'
	include '../lib/includes/stdlib.inc'

	;;   char *dirname(char *path)
	path equ arg1

dirname:
	enter 0, 0
	push  esi

	;   This alters path
	;   ecx holds pointer to end of the string (ignoring trailing /'s)
	;   edx holds pointer to after the last / or NULL
	mov esi, path
	mov ecx, NULL
	mov edx, NULL

	cmp esi, NULL
	je  .dot

.scan_loop:
	lodsb

	test al, al
	jne  .scan_loop

	mov ecx, esi
	mov esi, path
	dec ecx; null byte
	cmp ecx, esi; empty string
	je  .dot

.trim_loop:
	dec ecx
	cmp ecx, esi
	jle .trim_end
	cmp byte [ecx], '/'
	je  .trim_loop

.trim_end:
	;   if the path is only /'s then return path
	cmp ecx, esi
	jne .trim
	cmp byte [ecx], '/'
	je  .slash

.trim:
	inc ecx
	mov byte [ecx], NULL

.loop:
	lodsb

	test al, al
	je   .end

	cmp al, '/'
	jne .loop

	;   this was a slash, store it
	mov edx, esi
	dec edx

	jmp .loop

.end:
	;    check if slash found
	test edx, edx
	;    if not, then return "."
	je   .dot

	cmp edx, path
	je  .slash

	mov byte [edx], NULL
	mov eax, path
	jmp .return

.slash:
	mov eax, .slash_str
	jmp .return

.dot:
	mov eax, .dot_str

.return:
	pop esi
	leave
	ret

.dot_str: db ".", NULL
.slash_str: db "/", NULL

	;;   char *basename(char *path)
	path equ arg1

basename:
	enter 0, 0
	push  esi

	;   This alters path
	;   ecx holds pointer to end of the string (ignoring trailing /'s)
	;   edx holds pointer to after last / or NULL
	mov esi, path
	mov ecx, NULL
	mov edx, NULL

	cmp esi, NULL
	je  .dot

.scan_loop:
	lodsb

	test al, al
	jne  .scan_loop

	mov ecx, esi
	mov esi, path
	dec ecx; null byte
	cmp ecx, esi; empty string
	je  .dot

.trim_loop:
	dec ecx
	cmp ecx, esi
	jle .trim_end
	cmp byte [ecx], '/'
	je  .trim_loop

.trim_end:
	;   if the path is only /'s then return path
	cmp ecx, esi
	jne .trim
	cmp byte [ecx], '/'
	je  .slash

.trim:
	inc ecx
	mov byte [ecx], NULL

.loop:
	lodsb

	test al, al
	je   .end

	cmp al, '/'
	jne .loop

	;   this was a slash, store it (points after it)
	mov edx, esi

	jmp .loop

.end:
	;     check if slash found, return path if not
	mov   eax, edx
	test  eax, eax
	cmove eax, path
	jmp   .return

.slash:
	mov eax, .slash_str
	jmp .return

.dot:
	mov eax, .dot_str

.return:
	pop esi
	leave
	ret

.dot_str: db ".", NULL
.slash_str: db "/", NULL
