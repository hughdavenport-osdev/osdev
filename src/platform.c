#include "lib/includes/libgen.h"
#include "lib/includes/ring_buffer.h"
#include "lib/includes/stdio.h"
#include "lib/includes/stdlib.h"
#include "lib/includes/string.h"
#include "lib/includes/termios.h"
#include "lib/includes/unistd.h"
#define UNIMPL(s)                 \
    void s(void* ignore)          \
    {                             \
        (void)ignore;             \
        printf("TODO: %s\n", #s); \
    }

void panic(const char* s);

void music_add_builtin(void) { }
void sched_add_builtin(void) { }
void term_add_builtin(void) { }

UNIMPL(vga_enable_last_printed_str)
UNIMPL(vga_disable_last_printed_str)
UNIMPL(vga_escape_str)
UNIMPL(debug_add_function)
UNIMPL(new_task)
UNIMPL(schedule)
UNIMPL(start_task)
UNIMPL(task_setup_switch_to_task_stack)
UNIMPL(term_readkey_handle)
UNIMPL(vga_putchar_at)
UNIMPL(vga_set_cursor)

void* memory_chunks[1024 * 1024] = {};

void* memory_get_chunk(size_t* size_ptr)
{
    // FIXME: should be %lu but that causes segfault in atoi (for now)...
    (void)size_ptr;
    //    printf("allocating memory chunk of size %u\n", *size_ptr);
    return memory_chunks;
}

typedef struct {
    int pid;
    void* stack_top;
    void* esp;
    int cr3;
    void* next;
    int state;
    int file_descriptors_len;
    FILE** file_descriptors;
} task;
task hack_current_task_s = { .file_descriptors_len = 0, .file_descriptors = 0 };
task* hack_current_task = &hack_current_task_s;
void* hack_term_ptr;
void* pit_ms;

#define __NR_exit 1
void exit_platform(int ret)
{
    asm volatile(
        "int $0x80"
        : "=a"(ret)
        : "0"(__NR_exit), "b"(ret));
}

typedef int ssize_t;
#define __NR_read 3
ssize_t read_platform(int fd, const void* buf, size_t size)
{
    ssize_t ret;
    asm volatile(
        "int $0x80"
        : "=a"(ret)
        : "0"(__NR_read), "b"(fd), "c"(buf), "d"(size)
        : "memory" // the kernel dereferences pointer args
    );
    return ret;
}

#define __NR_write 4
ssize_t
_write_platform(int fd, const void* buf, size_t size)
{
    ssize_t ret;
    asm volatile(
        "int $0x80"
        : "=a"(ret)
        : "0"(__NR_write), "b"(fd), "c"(buf), "d"(size)
        : "memory" // the kernel dereferences pointer args
    );
    return ret;
}
int platform_write_utf8(int fd, unsigned int code)
{
#define write_return()                          \
    {                                           \
        size_t ret;                             \
        ret = _write_platform(fd, bytes, size); \
        return ret > 0 ? ret : -1;              \
    }

#define next_byte(start, mask, bits)                    \
    {                                                   \
        char b = (start);                               \
        while ((b & 0x80) == 0) {                       \
            b = (b << 1) | ((code & (mask)) >> (bits)); \
            code <<= 1;                                 \
        }                                               \
        bytes[size++] = b;                              \
    }

    unsigned char bytes[4] = {};
    size_t size = 0;
    if (code <= 0x7F) {
        // just ascii, take 7 bits
        bytes[size++] = code;
        write_return();
    } else if (code <= 0x7FF) {
        // take 11 bits
        next_byte(0b110, 0x7FF, 10);
        next_byte(0b10, 0x7FF, 10);
        write_return();
    } else if (code <= 0xFFFF) {
        // take 16 bits
        next_byte(0b1110, 0xFFFF, 15);
        next_byte(0b10, 0xFFFF, 15);
        next_byte(0b10, 0xFFFF, 15);
        write_return();
    }
    // TODO larger codepoints, next_byte assumes 16bit
    printf("printing unicode 0x%x\n", code);
    panic("unimpl utf8 code");
    return -1;
}

unsigned int codepage437_low[] = {
    0,
    0x263A,
    0x263B,
    0x2665,
    0x2666,
    0x2663,
    0x2660,
    0x2022,
    0x25D8,
    0x25CB,
    0x25D9,
    0x2642,
    0x2640,
    0x266A,
    0x266B,
    0x263C,
    0x25BA,
    0x25C4,
    0x2195,
    0x203C,
    0x00B6,
    0x00A7,
    0x25AC,
    0x21A8,
    0x2191,
    0x2193,
    0x2192,
    0x2190,
    0x221F,
    0x2194,
    0x25B2,
    0x25BC,
};

// from 0x80
unsigned int codepage437_high[] = {
    0x00C7,
    0x00FC,
    0x00E9,
    0x00E2,
    0x00E4,
    0x00E0,
    0x00E5,
    0x00E7,
    0x00EA,
    0x00EB,
    0x00E8,
    0x00EF,
    0x00EE,
    0x00EC,
    0x00C4,
    0x00C5,
    0x00C9,
    0x00E6,
    0x00C6,
    0x00F4,
    0x00F6,
    0x00F2,
    0x00FB,
    0x00F9,
    0x00FF,
    0x00D6,
    0x00DC,
    0x00A2,
    0x00A3,
    0x00A5,
    0x20A7,
    0x0192,
    0x00E1,
    0x00ED,
    0x00F3,
    0x00FA,
    0x00F1,
    0x00D1,
    0x00AA,
    0x00BA,
    0x00BF,
    0x2310,
    0x00AC,
    0x00BD,
    0x00BC,
    0x00A1,
    0x00AB,
    0x00BB,
    0x2591,
    0x2592,
    0x2593,
    0x2502,
    0x2524,
    0x2561,
    0x2562,
    0x2556,
    0x2555,
    0x2563,
    0x2551,
    0x2556,
    0x255D,
    0x255C,
    0x255B,
    0x2510,
    0x2514,
    0x2534,
    0x252C,
    0x251C,
    0x2500,
    0x253C,
    0x255E,
    0x255F,
    0x255A,
    0x2554,
    0x2569,
    0x2566,
    0x2560,
    0x2550,
    0x256C,
    0x2567,
    0x2568,
    0x2564,
    0x2565,
    0x2559,
    0x2558,
    0x2552,
    0x2553,
    0x256B,
    0x256A,
    0x2518,
    0x250C,
    0x2588,
    0x2584,
    0x258C,
    0x2590,
    0x2580,
    0x03B1,
    0x00DF,
    0x0393,
    0x03C0,
    0x03A3,
    0x03C3,
    0x00B5,
    0x03C4,
    0x03A6,
    0x0398,
    0x03A9,
    0x03B4,
    0x221E,
    0x03C6,
    0x03B5,
    0x2229,
    0x2261,
    0x00B1,
    0x2265,
    0x2264,
    0x2320,
    0x2321,
    0x00F7,
    0x2248,
    0x00B0,
    0x2219,
    0x00B7,
    0x221A,
    0x207F,
    0x00B2,
    0x25A0,
    0x00A0,
};

int platform_is_codepage437(unsigned char c)
{
    return (c >= 0x1 && c < 0x7)
        || (c >= 0xe && c < 0x20 && c != 0x1b)
        || c > 0x7f;
}

ssize_t
platform_write_codepage437(int fd, unsigned char c)
{
    if (!((c >= 0 && c < 32) || c > 127)) {
        return 0;
    }
    // TODO have some array
    if (c < 32) {
        platform_write_utf8(fd, codepage437_low[(size_t)c]);
    } else if (c > 127) {
        platform_write_utf8(fd, codepage437_high[(size_t)(c - 128)]);
    }
    return 1;
}

ssize_t
write_platform(int fd, const unsigned char* buf, size_t size)
{
    ssize_t ret;
    // Check if outside of ascii, or control characters we respect(?)
    for (size_t i = 0; i < size; i++) {
        unsigned char c = buf[i];
        if (platform_is_codepage437(c)) {
            ret = 0;
            if (i > 0) {
                ret += write_platform(fd, buf, i);
            }
            do {
                c = buf[i];
                platform_write_codepage437(fd, c);
                ret++;
                i++;
            } while (i < size && platform_is_codepage437(buf[i]));
            if (i < size) {
                ret += write_platform(fd, buf + i, size - i);
            }
            return ret;
        }
    }
    return _write_platform(fd, buf, size);
}

typedef long time_t;
struct timespec {
    time_t tv_sec;
    long tv_nsec;
};
#define __NR_nanosleep 162
int nanosleep(const struct timespec* req, struct timespec* rem)
{
    int ret;
    asm volatile(
        "int $0x80"
        : "=a"(ret)
        : "0"(__NR_nanosleep), "b"(req), "c"(rem)
        : "memory");
    return ret;
}

int __wrap_usleep(useconds_t usec)
{
    struct timespec req = { .tv_nsec = usec * 1000 };
    struct timespec rem = {};
    while (nanosleep(&req, &rem) != 0) {
        req = rem;
        rem.tv_nsec = 0;
    };
    return 0;
}

unsigned int __wrap_sleep(unsigned int seconds)
{
    struct timespec req = { .tv_sec = seconds };
    struct timespec rem = {};
    while (nanosleep(&req, &rem) != 0) {
        req = rem;
        rem.tv_sec = 0;
        rem.tv_nsec = 0;
    };
    return 0;
}

int putc_platform(int c, FILE* stream)
{
    int fd = -1;
    if (stream == stdout) {
        fd = STDOUT_FILENO;
    } else if (stream == stderr) {
        fd = STDERR_FILENO;
    }
    if (fd < 0) {
        panic("Invalid output stream");
    }
    if (platform_is_codepage437(c)) {
        return platform_write_codepage437(fd, c);
    } else {
        char buf[1] = { (char)c };
        // TODO buffer for speed
        return _write_platform(fd, buf, 1);
    }
}

#define EAGAIN -11
void* stdin_object;
int getc_platform(FILE* stream)
{
    int fd = -1;
    if (stream == stdin_object) {
        fd = STDIN_FILENO;
    }
    if (fd < 0) {
        panic("Invalid input stream");
    }
    char buf[1] = {};
    int ret = ring_buffer_getc(stdin_object);
    if (ret != -1) {
        return ret;
    }
    // TODO buffer for speed
    ssize_t size;
    while ((size = read_platform(fd, buf, 1)) != 1) {
        if (size == EAGAIN)
            continue;
        panic("Could not read");
    }
    // printf("read: %d \'%c\'\n", buf[0], buf[0]);
    return buf[0];
}

void panic(const char* s)
{
    printf("PANIC: %s\n", s);
    exit_platform(1);
}

int task_add_file(FILE* stream)
{
    FILE** descs = hack_current_task->file_descriptors;
    hack_current_task->file_descriptors_len++;
    descs = realloc(descs, sizeof(FILE*) * hack_current_task->file_descriptors_len);
    descs[hack_current_task->file_descriptors_len - 1] = stream;
    hack_current_task->file_descriptors = descs;
    return hack_current_task->file_descriptors_len - 1;
}

#define __NR_fcntl 55
#define F_SETFL 4
#define O_NONBLOCK 2048
int set_nonblocking(int fd)
{
    ssize_t ret;
    asm volatile(
        "int $0x80"
        : "=a"(ret)
        : "0"(__NR_fcntl), "b"(fd), "c"(F_SETFL), "d"(O_NONBLOCK)
        : "memory" // the kernel dereferences pointer args
    );
    return ret;
}

void platform_setup_stdio()
{
    FILE** descs = calloc(3, sizeof(FILE*));
    ((FILE*)stdout)->object = stdout;
    ((FILE*)stdout)->putc = putc_platform;
    ((FILE*)stderr)->object = stderr;
    ((FILE*)stderr)->putc = putc_platform;

    // setup taken from kmain()
    stdin_object = (void*)ring_buffer_new();
    ((FILE*)stdin)->object = stdin_object;
    ((FILE*)stdin)->putc = ring_buffer_putc;
    ((FILE*)stdin)->getc = getc_platform;
    ((FILE*)stdin)->ungetc = ring_buffer_ungetc;
    ((FILE*)stdin)->termios = (void*)termios_new(stdin);

    set_nonblocking(STDIN_FILENO);

    descs[0] = stdin;
    descs[1] = stdout;
    descs[2] = stderr;
    hack_current_task->file_descriptors = descs;
    hack_current_task->file_descriptors_len = 3;
}

void platform_cleanup_stdio()
{
    free(hack_current_task->file_descriptors);
    hack_current_task->file_descriptors = NULL;
    hack_current_task->file_descriptors_len = 0;
}

extern void shell_main();
extern void hello_main();
int platform_main(int argc, char** argv)
{
    char* progname = basename(argv[0]);

    (void)argc;
    (void)argv;
    // printf("argc = %d\n", argc);
    // for (int i = 0; i < argc; ++i) {
    //     printf("argc[%d] = \"%s\"\n", i, argv[i]);
    // }

    if (strcmp(progname, "shell") == 0) {
        shell_main();
    } else if (strcmp(progname, "hello") == 0) {
        hello_main();
    } else {
        printf("Unknown program \"%s\"\n", progname);
        panic("Need to add new programs to platform.c");
    }

    // TODO get return code from util mains
    return -1;
}

void _start(void)
{
    void* ebp = NULL;
    asm volatile(
        "mov %%ebp, %0"
        : "=m"(ebp));
    int argc = *(int*)(ebp + 4);
    char** argv = (char**)(ebp + 8);

    platform_setup_stdio();
    int ret = platform_main(argc, argv);
    platform_cleanup_stdio();

    exit_platform(ret);
}
