#include "../lib/includes/stdio.h"
#include "../lib/includes/string.h"
#include "../lib/includes/termios.h"
#include "../lib/includes/unistd.h"

extern void panic();

#define BUF_CAP 128
#define TEST_FGETS_OR_PANIC(expected) ({                                            \
    printf("\nEnter the string '%s': ", expected);                                  \
    char buf[BUF_CAP];                                                              \
    char* ret;                                                                      \
    while ((ret = fgets(buf, BUF_CAP, stdin)) == NULL)                              \
        ;                                                                           \
    if (strcmp(ret, expected) != 0) {                                               \
        printf("\nERROR: expected \"%s\" on fgets(), got \"%s\"\n", expected, ret); \
        panic();                                                                    \
    }                                                                               \
    ret;                                                                            \
})

void test_fgets()
{
    puts("Testing char *fgets(char *s, int size, FILE *stream)\n");
    puts("Resetting termio, really should be done in shell...\n");
    struct termios old = { 0 };
    if (tcgetattr(STDIN_FILENO, &old) < 0) {
        puts("Failed tcgetattr\n");
        panic();
    }
    // TODO function to reset, and should be done in shell
    struct termios new = old;
    new.c_lflag |= ICANON;
    new.c_lflag |= ECHO;
    if (tcsetattr(STDIN_FILENO, TCSANOW, &new) < 0) {
        puts("Failed tcsetattr\n");
        panic();
    }

    char* ret;
    char buf[BUF_CAP];
    ret = fgets(buf, BUF_CAP, NULL);
    if (ret != NULL) {
        printf("ERROR: expected NULL/EOF on fgets(stream=NULL), got \"%s\"\n", ret);
        panic();
    }
    ret = fgets(NULL, BUF_CAP, NULL);
    if (ret != NULL) {
        printf("ERROR: expected NULL on fgets(s=NULL), got \"%s\"\n", ret);
        panic();
    }
    ret = fgets(buf, 0, NULL);
    if (ret != NULL) {
        printf("ERROR: expected NULL on fgets(size=0), got \"%s\"\n", ret);
        panic();
    }
    TEST_FGETS_OR_PANIC("Hello World!\n");
    TEST_FGETS_OR_PANIC("Test123\n");
    ret = fgets(buf, BUF_CAP, NULL);
    if (ret != NULL) {
        printf("ERROR: expected NULL/EOF on fgets(), got \"%s\"\n", ret);
        panic();
    }
    puts("\nAll fgets tests passed, press Enter to continue");
    TEST_FGETS_OR_PANIC("\n");

    puts("Resetting termios, should really be done in shell...\n");
    if (tcsetattr(STDIN_FILENO, TCSANOW, &old) < 0) {
        puts("Failed tcsetattr\n");
        panic();
    }
    puts("Finished testing fgets\n");
}
