#include "../lib/includes/ctype.h"
#include "../lib/includes/stdio.h"
extern void panic();

#define TEST_ISDIGIT_OR_PANIC(c, expected) ({                                        \
    int r = isdigit(c);                                                              \
    if (r != expected) {                                                             \
        printf("isdigit: Failed isdigit(0x%x) = %d, expected %d\n", c, r, expected); \
        panic();                                                                     \
    }                                                                                \
    r;                                                                               \
})
#define TEST_ISDIGIT_TRUE_OR_PANIC(c) TEST_ISDIGIT_OR_PANIC(c, 1)
#define TEST_ISDIGIT_FALSE_OR_PANIC(c) TEST_ISDIGIT_OR_PANIC(c, 0)

void test_isdigit()
{
    puts("Testing int isdigit(int c)\n");
    puts("\tTesting that digits return true: ");
    TEST_ISDIGIT_TRUE_OR_PANIC('0');
    TEST_ISDIGIT_TRUE_OR_PANIC('1');
    TEST_ISDIGIT_TRUE_OR_PANIC('2');
    TEST_ISDIGIT_TRUE_OR_PANIC('3');
    TEST_ISDIGIT_TRUE_OR_PANIC('4');
    TEST_ISDIGIT_TRUE_OR_PANIC('5');
    TEST_ISDIGIT_TRUE_OR_PANIC('6');
    TEST_ISDIGIT_TRUE_OR_PANIC('7');
    TEST_ISDIGIT_TRUE_OR_PANIC('8');
    TEST_ISDIGIT_TRUE_OR_PANIC('9');
    puts("OK\n");
    puts("\tTesting that other characters return false: ");
    for (int c = 0; c < '0'; ++c) {
        TEST_ISDIGIT_FALSE_OR_PANIC(c);
    }
    for (int c = '9' + 1; c < 1025; ++c) {
        TEST_ISDIGIT_FALSE_OR_PANIC(c);
    }
    puts("OK\n");
    puts("Finished testing isdigit\n");
}
