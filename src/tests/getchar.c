#include "../lib/includes/stdio.h"
#include "../lib/includes/termios.h"
#include "../lib/includes/unistd.h"

extern void panic();

#define TEST_GETCHAR_OR_PANIC(expected) ({                                                  \
    printf("\nEnter the char '%c': ", expected);                                            \
    int ret;                                                                                \
    if ((ret = getchar()) != expected) {                                                    \
        printf("\nERROR: expected '%c' on getchar(), got '%c' (%d)\n", expected, ret, ret); \
        panic();                                                                            \
    }                                                                                       \
    getchar(); /*the newline*/                                                              \
    ret;                                                                                    \
})

void test_getchar()
{
    puts("Testing int getchar()\n");
    puts("Resetting termio, really should be done in shell...\n");
    struct termios old = { 0 };
    if (tcgetattr(STDIN_FILENO, &old) < 0) {
        puts("Failed tcgetattr\n");
        panic();
    }
    // TODO function to reset, and should be done in shell
    struct termios new = old;
    new.c_lflag |= ICANON;
    new.c_lflag |= ECHO;
    if (tcsetattr(STDIN_FILENO, TCSANOW, &new) < 0) {
        puts("Failed tcsetattr\n");
        panic();
    }

    TEST_GETCHAR_OR_PANIC('a');
    TEST_GETCHAR_OR_PANIC('X');
    TEST_GETCHAR_OR_PANIC('~');
    TEST_GETCHAR_OR_PANIC('8');
    TEST_GETCHAR_OR_PANIC('$');
    TEST_GETCHAR_OR_PANIC('`');
    TEST_GETCHAR_OR_PANIC('[');
    TEST_GETCHAR_OR_PANIC('|');
    puts("\nAll getchar tests passed, press Enter to continue");
    getchar();

    puts("Resetting termios, should really be done in shell...\n");
    if (tcsetattr(STDIN_FILENO, TCSANOW, &old) < 0) {
        puts("Failed tcsetattr\n");
        panic();
    }
    puts("Finished testing getchar\n");
}
