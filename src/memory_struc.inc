	struc   memory_descriptor {
	.base_low    dd ?
	.base_high   dd ?
	.length_low  dd ?
	.length_high dd ?
	.type   dd ?
	.acpi   dd ?
	}
	virtual at esi
	memory_descriptor memory_descriptor
	sizeof.memory_descriptor = $ - memory_descriptor
	end     virtual
