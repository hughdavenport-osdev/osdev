	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  pic8259_init

	extrn ps2_keyboard_handler
	extrn pit_tick_handler
	extrn idt_add_trap_entry

	include "../includes/kernel.inc"

	PIC1_COMMAND = 0x20
	PIC1_DATA    = 0x21
	PIC2_COMMAND = 0xA0
	PIC2_DATA    = 0xA1

	PIC_EOI = 0x20 ; End-of-interrupt command code

	ICW1_ICW4      = 0x01 ; ICW4 (not) needed
	ICW1_SINGLE    = 0x02 ; Single (cascade) mode
	ICW1_INTERVAL4 = 0x04 ; Call address interval 4 (8)
	ICW1_LEVEL     = 0x08 ; Level triggered (edge) mode
	ICW1_INIT      = 0x10 ; Initialization - required!

	ICW4_8086          = 0x01 ; 8086/88 (MCS-80/85) mode
	ICW4_AUTO          = 0x02 ; Auto (normal) EOI
	ICW4_BUF_SECONDARY = 0x08 ; Buffered mode/secondary
	ICW4_BUF_PRIMARY   = 0x0C ; Buffered mode/primary
	ICW4_SFNM          = 0x10 ; Special fully nested (not)

	PIC1_OFFSET = 0x20
	PIC2_OFFSET = 0x28

	TIMER    = 0
	KEYBOARD = 1

	;      FIXME this public is just to get debug symbols
	public timer_handler

timer_handler:
	enter 0, 0
	pusha
	ccall pit_tick_handler
	ccall pic8259_send_eoi, TIMER
	popa
	leave
	iret

	; TODO make a common handler

	;      FIXME this public is just to get debug symbols
	public keyboard_handler

keyboard_handler:
	enter 0, 0
	pusha
	ccall ps2_keyboard_handler
	ccall pic8259_send_eoi, KEYBOARD
	popa
	leave
	iret

pic8259_send_eoi:
	enter 0, 0
	cmp   dword arg1, 8
	jl    .send_primary
	mov   al, PIC_EOI
	out   PIC2_COMMAND, al

.send_primary:
	mov al, PIC_EOI
	out PIC1_COMMAND, al
	leave
	ret

pic8259_init:
	ccall pic8258_initialize

	;     setup handlers
	ccall idt_add_trap_entry, PIC1_OFFSET + TIMER, timer_handler
	ccall idt_add_trap_entry, PIC1_OFFSET + KEYBOARD, keyboard_handler
	ret

pic8258_initialize:
	enter 0, 0
	in    al, PIC1_DATA
	push  eax
	in    al, PIC2_DATA
	push  eax

	;   Initialize the PICs
	mov al, ICW1_INIT or ICW1_ICW4
	out PIC1_COMMAND, al
	xor al, al
	out 0x80, al

	mov al, ICW1_INIT or ICW1_ICW4
	out PIC2_COMMAND, al
	xor al, al
	out 0x80, al

	;   Set the offsets
	mov al, PIC1_OFFSET
	out PIC1_DATA, al
	xor al, al
	out 0x80, al

	mov al, PIC2_OFFSET
	out PIC2_DATA, al
	xor al, al
	out 0x80, al

	;   Tell the primary PIC there is a secondary PIC
	mov al, 4
	out PIC1_DATA, al
	xor al, al
	out 0x80, al

	;   Tell the secondary PIC its cascade identity
	mov al, 2
	out PIC2_DATA, al
	xor al, al
	out 0x80, al

	mov al, ICW4_8086
	out PIC1_DATA, al
	xor al, al
	out 0x80, al

	mov al, ICW4_8086
	out PIC2_DATA, al
	xor al, al
	out 0x80, al

	pop eax
	out PIC2_DATA, al
	pop eax
	out PIC1_DATA, al
	leave
	ret
