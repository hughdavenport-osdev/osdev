#include "lib/includes/stdio.h"
// TODO: split these out into files
extern void test_itoa();
extern void test_atoi();
extern void test_isdigit();
extern void test_printf();
extern void test_strcmp();
extern void test_strlen();
extern void test_puts();
extern void test_strnrev();
extern void test_putchar();
extern void test_getchar();
extern void test_fgets();
extern void test_colour();
extern void test_strtok();
extern void test_malloc();
extern void test_charmap();

#define TODO(func)                           \
    {                                        \
        printf("TODO: test for %s\n", func); \
    }

// todo make a macro to
// - add extern
// - add to run_tests
// - add to builtins
void run_tests()
{
    puts("This was printed from test.c in run_tests()\n");
    puts("\e[1;5;31;42mBlink?\e[0m\n");

    TODO("putchar")
    TODO("memcpy")
    TODO("drivers (vga and ps2-keyboard)")
    test_printf();
    test_puts();
    test_putchar();
    test_itoa();
    test_atoi();
    test_isdigit();
    test_strcmp();
    test_strlen();
    test_strnrev();
    test_strtok();
    test_getchar();
    test_fgets();
    test_malloc();
    test_colour();
    test_charmap();
}

extern void shell_add_builtin(char* cmd_str, char* doc_str, void* cmd);
#define ADD_BUILTIN(s) shell_add_builtin(#s, #s, s);

void add_test_builtins()
{
    ADD_BUILTIN(test_colour);
    ADD_BUILTIN(test_printf);
    ADD_BUILTIN(test_puts);
    ADD_BUILTIN(test_putchar);
    ADD_BUILTIN(test_itoa);
    ADD_BUILTIN(test_atoi);
    ADD_BUILTIN(test_isdigit);
    ADD_BUILTIN(test_strcmp);
    ADD_BUILTIN(test_strlen);
    ADD_BUILTIN(test_strnrev);
    ADD_BUILTIN(test_strtok);
    ADD_BUILTIN(test_getchar);
    ADD_BUILTIN(test_fgets);
    ADD_BUILTIN(test_malloc);
    ADD_BUILTIN(test_charmap);
}
