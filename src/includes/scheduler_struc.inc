	struc  thread_control_block {
	.pid   dd ?
	.stack_top   dd ?
	.esp   dd ?
	;      TODO may need a TSS and have esp0
	.cr3   dd ?; not used?
	.next  dd ?
	.state dd ?
	.file_descriptors_len dd ?
	.file_descriptors dd ?
	;      TODO accounting and prios

	}
	virtual at edi
	thread_control_block thread_control_block
	sizeof.thread_control_block = $ - thread_control_block
	end     virtual
