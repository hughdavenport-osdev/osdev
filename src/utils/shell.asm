	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  shell_main

	include "../includes/kernel.inc"
	include "../lib/includes/stdio.inc"
	include "../lib/includes/stdlib.inc"
	include "../lib/includes/string.inc"
	include "../lib/includes/unistd.inc"
	include "../lib/includes/termios.inc"
	include "../lib/includes/execinfo.inc"
	include "../lib/includes/readkey.inc"
	include "../lib/includes/term.inc"
	include "../includes/ascii.inc"

	BUF_CAP = 4096
	; FIXME Increasing this causes the kernel to not be able to load everything..
	HISTORY_CAP = 50

	fd equ arg1
	echo_char   equ arg2

	escape_mode equ var1
	saved_command equ var2
	args_size = 2 * WORD_SIZE

	ESCAPE_MODE_NORMAL = 0
	ESCAPE_MODE_ESCAPE = 1
	ESCAPE_MODE_CSI    = 2

shell_process_command:
	enter args_size, 0
	push  ebx
	push  esi
	push  edi

	ccall memset, shell_command_buffer, NULL, BUF_CAP
	mov   escape_mode, 0
	mov   saved_command, 0

	mov esi, [history_list]

	;   TODO, for history and other fun stuff
	;   set raw mode with tcsetattr
	;   use getc(stream) one by one
	;   test for cursors
	;   will have to handle backspace etc
	;   TODO it would also be cool to have a command autosuggestion popup as you type
	;   that would need a prefix tree (trie) structure
	mov edi, shell_command_buffer
	mov ebx, 0

.loop:
	cmp   ebx, BUF_CAP
	jge   .overflow
	mov   eax, fd
	ccall readkey, eax
	cmp   eax, -1
	jz    .eof

	test eax, READKEY_HAS_FLAG_MASK
	jne  .readkey_flag

	cmp al, ASCII_NEWLINE
	je  .end_loop

	cmp al, ASCII_BACKSPACE
	je  .backspace

	cmp   dword echo_char, 0
	jz    .loop_inc
	push  eax
	ccall putchar, eax
	pop   eax

.loop_inc:
	stosb
	inc ebx
	jmp .loop

.eof:
	test ebx, ebx
	jz   .return_eof
	;    we are just waiting for user input
	jmp  .loop

.backspace:
	test ebx, ebx
	jz   .loop

	dec edi

	cmp   dword echo_char, 0
	jz    .clear_last_char
	ccall putchar, ASCII_BACKSPACE

	mov al, [edi]
	cmp al, ASCII_TAB
	jne .clear_last_char

	; printing a \b only moves the cursor back
	; if it was a tab before us, clear a few more
	; this assumes tab is exactly 4 spaces, which may not be correct
	; TODO in future, this will be some sort of readline function which will redraw line

	ccall putchar, ASCII_BACKSPACE
	ccall putchar, ASCII_BACKSPACE
	ccall putchar, ASCII_BACKSPACE

.clear_last_char:
	mov byte [edi], NULL
	dec ebx
	jmp .loop

.readkey_flag:
	;    TODO the window manager should really be responsible for this sort of passing
	test eax, READKEY_FLAG_FUNC_MASK
	jz   .not_func

	push  eax
	extrn hack_term_ptr
	ccall term_readkey_handle, [hack_term_ptr], eax
	pop   eax

.not_func:
	test eax, READKEY_FLAG_KEY_MASK
	jnz  .special_key

	; Unknown type

	jmp .loop

.special_key:
	cmp al, READKEY_PROCESSING
	je  .loop

	cmp al, READKEY_KEY_CURSOR_UP
	je  .cursor_up
	cmp al, READKEY_KEY_CURSOR_DOWN
	je  .cursor_down

	;   TODO add more
	jmp .loop

.cursor_up:
	cmp esi, [history_list]
	jne .no_save

	ccall strndup, shell_command_buffer, ebx
	mov   saved_command, eax

.no_save:
	dec esi
	jmp .clear_and_load_history

.cursor_down:
	inc esi
	jmp .clear_and_load_history

.clear_and_load_history:
	mov escape_mode, ESCAPE_MODE_NORMAL

	cmp esi, 0
	jl  .history_underflow

	cmp esi, [history_list]
	jg  .history_overflow

	std

.clear_current_command_loop:
	cmp edi, shell_command_buffer
	je  .get_history

	cmp   dword echo_char, 0
	jz    .del_char
	ccall putchar, ASCII_BACKSPACE

.del_char:
	mov al, NULL
	stosb
	jmp .clear_current_command_loop

.get_history:
	mov byte [edi], NULL
	cld

	cmp esi, [history_list]
	je  .get_saved_command

	mov   edi, [history_list + WORD_SIZE*(esi+1)]
	;     TODO it could be good to store the history length in the struct to avoid lots of strlens?
	ccall strlen, edi
	mov   ebx, eax
	ccall memcpy, shell_command_buffer, edi, ebx
	mov   edi, shell_command_buffer
	add   edi, ebx

	cmp   dword echo_char, 0
	jz    .loop
	ccall puts, shell_command_buffer

	jmp .loop

.get_saved_command:
	;     TODO this is very similar to most of what is above, can it get refactored?
	mov   edi, saved_command
	;     TODO it could be good to store the saved command length as well to avoid lots of strlens?
	ccall strlen, edi
	mov   ebx, eax
	ccall memcpy, shell_command_buffer, edi, ebx
	ccall free, edi
	mov   saved_command, NULL
	mov   edi, shell_command_buffer
	add   edi, ebx

	cmp   dword echo_char, 0
	jz    .loop
	ccall puts, shell_command_buffer

	jmp .loop

.history_underflow:
	;     TODO check some global config if should play bell
	ccall putchar, ASCII_BELL
	mov   esi, 0
	jmp   .loop

.history_overflow:
	;     TODO check some global config if should play bell
	ccall putchar, ASCII_BELL
	mov   esi, [history_list]
	jmp   .loop

.overflow:
	; TODO if overflowing and not just newline, then we should fetch more data in new buffer

.end_loop:
	cmp   dword echo_char, 0
	jz    .null_terminate_str
	ccall putnl

.null_terminate_str:

	;    If no command is entered, just return
	test ebx, ebx
	jz   .return

	;   overwrite the newline with null
	mov al, NULL
	stosb

	; add to history

	ccall shell_history_append, shell_command_buffer, ebx

	mov ebx, shell_command_buffer

	ccall strtok, ebx, delimiters_str
	mov   edi, eax
	;     ebx - command (not trimmed)
	;     edi - command
	;     todo do something differently if prefixed by delim
	mov   ecx, [builtins]
	mov   esi, builtins+WORD_SIZE

.builtins_loop:
	test  ecx, ecx
	je    .non_builtin
	push  ecx
	ccall strcmp, edi, [esi]
	pop   ecx
	cmp   eax, 0
	jne   .builtins_loop_inc
	;     run command
	mov   eax, [esi+2*WORD_SIZE]
	ccall eax
	;     see if should exit
	jmp   .return

.builtins_loop_inc:
	add esi, 3*WORD_SIZE
	dec ecx
	jmp .builtins_loop

.non_builtin:
	; todo run some other cmd

	ccall printf, .unknown_command_printf_str, edi

	jmp .unknown
.unknown_command_printf_str: db "Unknown command: %s", 10, 0

.return_eof:
	mov eax, -1
	jmp .return

.unknown:
	;   TODO remove from history
	mov eax, 1
	jmp .return

.success:
	; FIXME this is no longer used?

	;   TODO add command to history
	;   will need to make a copy in history_strings
	;   strtok also would have removed spaces...
	mov eax, 0

.return:
	push  eax
	mov   eax, saved_command
	test  eax, eax
	jz    ._return
	ccall free, eax

._return:
	pop eax
	pop edi
	pop esi
	pop ebx
	leave
	ret

old_termios equ var1
args_size = 1 * WORD_SIZE

shell_main:
	enter args_size, 0

	ccall shell_init

	ccall tcgetattr, STDIN_FILENO, .termios_p
	test  eax, eax
	jnz   .error

	ccall malloc, sizeof.termios
	test  eax, eax
	jz    .error

	mov   old_termios, eax
	ccall memcpy, eax, .termios_p, sizeof.termios

	ccall cfmakeraw, .termios_p
	ccall tcsetattr, STDIN_FILENO, TCSANOW, .termios_p
	test  eax, eax
	jnz   .error

.prompt:

	ccall display_prompt

	ccall shell_process_command, STDIN_FILENO, 1
	cmp   eax, 0
	jge   .prompt

	;     FIXME this errors out
	mov   eax, old_termios
	ccall tcsetattr, STDIN_FILENO, TCSANOW, eax
	test  eax, eax
	jnz   .error

.error:
	ccall puts, .error_str

.return:
	leave
	ret

.error_str:
	db "ERROR using tcgetattr", 13, 10, 0

.termios_p:
	times sizeof.termios db 0

display_prompt:
	enter 0, 0
	ccall printf, ps1_str
	leave
	ret

	;; char *shell_next_arg()

shell_next_arg:
	enter 0, 0
	;     TODO this doesn't allow us to have '\ ' which would be useful...
	;     This relies on the fact that we use strtok to split from the first command
	;     FIXME in future, args will be parsed for us and put in an array somewhere?
	ccall strtok, NULL, delimiters_str

.return:
	leave
	ret

	BUILTINS_CAP = 30

builtins:
	dd    0
	times BUILTINS_CAP*3 dd 0

	cmd_str equ arg1
	shortdoc_str equ arg2
	cmd equ arg3

shell_add_builtin:
	enter 0, 0
	mov   eax, [builtins]
	cmp   eax, BUILTINS_CAP
	jge   .error
	mov   ecx, 3 * WORD_SIZE
	mul   ecx
	add   eax, builtins
	add   eax, WORD_SIZE
	mov   edx, cmd_str
	mov   [eax], edx
	add   eax, WORD_SIZE
	mov   edx, shortdoc_str
	mov   [eax], edx
	add   eax, WORD_SIZE
	mov   edx, cmd
	mov   [eax], edx
	add   eax, WORD_SIZE
	inc   dword [builtins]
	jmp   .return

.error:
	ccall printf, .builtin_overflow_str
	extrn panic
	ccall panic
	jmp   .return

.builtin_overflow_str:
	db "ERROR: Overflow in shell_add_builtin", 13, 10, 0

.return:
	leave
	ret

extrn run_tests

initialise_builtins:
	enter 0, 0
	ccall shell_add_builtin, help_cmd_str, help_shortdoc_str, help
	ccall shell_add_builtin, history_cmd_str, history_shortdoc_str, history
	ccall shell_add_builtin, noop_cmd_str, noop_shortdoc_str, noop
	ccall shell_add_builtin, echo_cmd_str, echo_shortdoc_str, echo
	ccall shell_add_builtin, printf_cmd_str, printf_shortdoc_str, shell_printf
	ccall shell_add_builtin, sleep_cmd_str, sleep_shortdoc_str, shell_sleep
	ccall shell_add_builtin, beep_cmd_str, beep_shortdoc_str, beep
	ccall shell_add_builtin, run_tests_cmd_str, run_tests_shortdoc_str, run_tests
	ccall shell_add_builtin, exit_cmd_str, exit_shortdoc_str, exit
	;     TODO add individual tests

.return:
	leave
	ret

history:
	enter 0, 0
	push  ebx

	xor ecx, ecx
	mov ebx, [history_list]

.history_loop:
	cmp ecx, ebx
	jge .return

	mov   edx, [history_list + WORD_SIZE*(ecx+1)]
	push  ecx
	ccall puts, edx
	ccall putnl
	pop   ecx

	inc ecx
	jmp .history_loop

.return:

	pop ebx
	leave
	ret

noop:
	ret

echo:
	enter 0, 0
	push  esi
	push  ebx
	xor   ebx, ebx

.loop:
	ccall shell_next_arg
	mov   esi, eax
	test  esi, esi
	jz    .return
	test  ebx, ebx
	je    .puts
	ccall putchar, ' '

.puts:
	ccall puts, esi
	inc   ebx
	jmp   .loop

.return:
	ccall putnl
	pop   ebx
	pop   esi
	leave
	ret

shell_printf:
	enter 0, 0
	push  ebx
	push  esi
	push  edi
	xor   ebx, ebx
	ccall shell_next_arg
	test  eax, eax
	jz    .usage
	mov   esi, eax

.parse_format_loop:
	mov  edx, esi
	xor  eax, eax
	lodsb
	test al, al
	je   .return
	cmp  al, '%'
	je   .parse_format
	cmp  al, '\'
	je   .parse_escape
	jmp  .putchar

.parse_format:
	;    TODO skip flags, field width, precision, length modifier
	;    FIXME parse * args
	lodsb
	test al, al
	je   .missing_format_error
	cmp  al, 'd'
	je   .integer_type
	cmp  al, 'i'
	je   .integer_type
	cmp  al, 'o'
	je   .integer_type
	cmp  al, 'u'
	je   .integer_type
	cmp  al, 'x'
	je   .integer_type
	cmp  al, 's'
	je   .string_type
	cmp  al, 'c'
	je   .char_type
	;    TODO %b and %q
	jmp  .invalid_format_error

.integer_type:
	mov   ebx, edx
	ccall shell_next_arg
	test  eax, eax
	je    .missing_argument_error
	ccall atoi, eax
	;     TODO detect error and print error about invalid number
	mov   edx, ebx
	jmp   .call_printf_with_format

.string_type:
	mov   ebx, edx
	ccall shell_next_arg
	test  eax, eax
	je    .missing_argument_error
	mov   edx, ebx
	jmp   .call_printf_with_format

.char_type:
	mov   ebx, edx
	ccall shell_next_arg
	test  eax, eax
	je    .missing_argument_error
	mov   edi, eax
	ccall strlen, eax
	test  eax, eax
	je    .missing_argument_error
	cmp   eax, 1
	jg    .char_too_long_error
	xor   eax, eax
	mov   al, [edi]
	mov   edx, ebx
	jmp   .call_printf_with_format

.call_printf_with_format:
	mov   bl, [esi]
	mov   byte [esi], NULL
	ccall printf, edx, eax
	mov   [esi], bl
	jmp   .parse_format_loop

.parse_escape:
	lodsb
	test al, al
	;    fixme, this could be backspace space, but next arg splits on whitespace, prob need to fix next_arg code

	;je   .missing_escape_code_error
	;     HACK: hack to do backslash space
	jne   .test_char
	ccall shell_next_arg
	cmp   eax, esi
	jne   .missing_escape_code_error
	xor   eax, eax
	mov   al, ' '
	mov   [esi-1], al

.test_char:
	;   todo, should this be in here, or swallowed by the shell arg parsing?
	cmp al, ' '
	je  .putchar
	cmp al, '"'
	je  .putchar
	cmp al, '\'
	je  .putchar
	cmp al, 'a'
	je  .putchar_bell
	cmp al, 'b'
	je  .putchar_backspace
	cmp al, 'c'
	je  .return; \c produces no further output
	cmp al, 'e'
	je  .putchar_escape
	cmp al, 'f'
	je  .putchar_form_feed
	cmp al, 'n'
	je  .putnl
	cmp al, 'r'
	je  .putchar_carriage_return
	cmp al, 't'
	je  .putchar_tab
	cmp al, 'v'
	je  .putchar_vertical_tab
	;   TODO \NNN, \xHH, \uHHHH, \UHHHHHHHH
	jmp .invalid_escape_code_error

.putchar:
	ccall putchar, eax
	jmp   .parse_format_loop

.putnl:
	ccall putnl
	jmp   .parse_format_loop

.putchar_bell:
	ccall putchar, ASCII_BELL
	jmp   .parse_format_loop

.putchar_backspace:
	ccall putchar, ASCII_BACKSPACE
	jmp   .parse_format_loop

.putchar_escape:
	ccall putchar, ASCII_ESCAPE
	jmp   .parse_format_loop

.putchar_form_feed:
	ccall putchar, ASCII_FORM_FEED
	jmp   .parse_format_loop

.putchar_carriage_return:
	ccall putchar, ASCII_CARRIAGE_RETURN
	jmp   .parse_format_loop

.putchar_tab:
	ccall putchar, ASCII_TAB
	jmp   .parse_format_loop

.putchar_vertical_tab:
	ccall putchar, ASCII_VERTICAL_TAB
	jmp   .parse_format_loop

.return:
	pop edi
	pop esi
	pop ebx
	leave
	ret

.usage:
	ccall puts, .usage_str
	jmp   .return

.usage_str:
	db "printf: usage: printf format [arguments]", 13, 10, 0

.arg_overflow:
	ccall printf, .overflow_str, ebx
	jmp   .return

.overflow_str:
	db "printf: too many arguments, max %d", 13, 10, 0
	;  inner function, call it

.null_terminate:
	mov edi, esi
	mov al, NULL
	stosb
	ret

.missing_format_error:
	;     TODO print char count?
	push  edx
	call  .null_terminate
	pop   edx
	ccall printf, .missing_format_error_str, edx
	jmp   .return

.missing_format_error_str:
	db "printf: '%s': missing format character", 13, 10, 0

.invalid_format_error:
	;     TODO print char count?
	push  edx
	call  .null_terminate
	pop   edx
	ccall printf, .invalid_format_error_str, edx
	jmp   .return

.invalid_format_error_str:
	db "printf: '%s': invalid format character", 13, 10, 0

.missing_argument_error:
	;     TODO print char count?
	call  .null_terminate
	ccall printf, .missing_argument_error_str, ebx
	jmp   .return

.missing_argument_error_str:
	db "printf: '%s': missing argument", 13, 10, 0

.char_too_long_error:
	;     TODO print char count?
	push  edi
	call  .null_terminate
	pop   edi
	ccall printf, .char_too_long_error_str, ebx, edi
	jmp   .return

.char_too_long_error_str:
	db "printf: '%s': expected single character, got '%s'", 13, 10, 0

.missing_escape_code_error:
	;     TODO print char count?
	ccall printf, .missing_escape_code_error_str
	jmp   .return

.missing_escape_code_error_str:
	db "printf: '\': missing escape code", 13, 10, 0

.invalid_escape_code_error:
	;     TODO print char count?
	ccall printf, .invalid_escape_code_error_str, eax
	jmp   .return

.invalid_escape_code_error_str:
	db "printf: '\%c': invalid escape code", 13, 10, 0

shell_sleep:
	enter 0, 0
	ccall shell_next_arg
	test  eax, eax
	jz    .usage
	ccall atoi, eax
	;     TODO detect invalid numbers
	;     TODO detect suffixes
	ccall sleep, eax

.return:
	leave
	ret

.usage:
	ccall puts, .usage_str
	jmp   .return

.usage_str:
	db "sleep: missing time argument", 13, 10, 0

beep:
	enter 0, 0
	ccall putchar, ASCII_BELL

.return:
	leave
	ret

exit:
	enter 0, 0
	mov   eax, -1
	leave
	ret

help:
	enter 0, 0
	push  ebx
	push  esi

	; todo see if we have an arg, and if so give more detailed help

	ccall puts, .help_str

	mov ebx, [builtins]
	mov esi, builtins + WORD_SIZE

.builtins_loop:
	test  ebx, ebx
	je    .return
	ccall printf, .help_printf_str, [esi], [esi+WORD_SIZE]

.builtins_loop_inc:
	add esi, 3*WORD_SIZE
	dec ebx
	jmp .builtins_loop

.return:
	pop esi
	pop ebx
	leave
	ret

.help_str:
	db "The following shell commands are builtin. Type `help` to see this list.", 13, 10
	db 13, 10
	db 0

	; width is 80 chars
	; TODO, used %-N.Ns to truncate longer strings, while still having min
	; this is currently not working in our printf

.help_printf_str:
	db " %-15s - %-60s", 13, 10, 0

	public shell_next_arg
	public shell_add_builtin

shell_init:
	enter 0, 0
	push  ebx

	ccall puts, .init_str

	cmp   byte [.initialised_builtins], 0
	jne   .initialised
	ccall initialise_builtins

	;     todo, these aren't really builtins
	extrn add_test_builtins
	ccall add_test_builtins
	extrn music_add_builtin
	ccall music_add_builtin
	extrn sched_add_builtin
	ccall sched_add_builtin
	extrn term_add_builtin
	ccall term_add_builtin

	mov byte [.initialised_builtins], 1

.initialised:

	ccall open_shell_autostart_file
	test  eax, eax
	jz    .return

	extrn task_add_file
	ccall task_add_file, eax
	mov   ebx, eax

.autostart_command:
	ccall shell_process_command, ebx, 0
	cmp   eax, 0
	jge   .autostart_command
	;eof

	; TODO clear history

.return:
	pop ebx
	leave
	ret

.init_str:
	db "Thank you for trying out my hobby OS.", 13, 10
	db "If you have any issues, please report a bug at the following bug tracker:", 13, 10
	db 9, "https://gitlab.com/hughdavenport-osdev/osdev", 13, 10
	db 0

.initialised_builtins:
	db 0

	;; void shell_history_append(const char* s, size_t n)

	s equ arg1
	n equ arg2

shell_history_append:
	enter 0, 0
	push  edi

	cmp dword [history_list], HISTORY_CAP
	jge .history_overflow

	mov   eax, s
	mov   ecx, n
	ccall strndup, eax, ecx
	test  eax, eax
	jz    .error
	mov   edi, eax

	inc  dword [history_list]
	mov  eax, [history_list]
	imul eax, 4
	add  eax, history_list
	mov  [eax], edi

	jmp .return

.history_overflow:
	; TODO Overflow/wrap history

.error:
	; TODO some error handling

.return:
	pop edi
	leave
	ret

history_cmd_str:
	db "history", 0

history_shortdoc_str:
	db "History command.", 0

noop_cmd_str:
	db ":", 0

noop_shortdoc_str:
	db "Null command.", 0

echo_cmd_str:
	db "echo", 0

echo_shortdoc_str:
	db "Write arguments to the output.", 0

printf_cmd_str:
	db "printf", 0

printf_shortdoc_str:
	db "Print arguments with format.", 0

sleep_cmd_str:
	db "sleep", 0

sleep_shortdoc_str:
	db "Delay for specified time", 0

beep_cmd_str:
	db "beep", 0

beep_shortdoc_str:
	db "Make an audible beep", 0

run_tests_cmd_str:
	db "run_tests", 0

run_tests_shortdoc_str:
	db "Run full test suite.", 0

exit_cmd_str:
	db "exit", 0

exit_shortdoc_str:
	db "Exit the shell", 0

help_cmd_str:
	db "help", 0

help_shortdoc_str:
	db "Display information about builtin commands.", 0

ps1_str:
	db "> ", 0

shell_command_buffer:
	times BUF_CAP db 0

delimiters_str:
	db " ", 9, 13, 13, 0

history_list:
	dd    0; count
	times HISTORY_CAP dd 0

	include "../lib/includes/ring_buffer.inc"

open_shell_autostart_file:
	;     todo, this will be a `open` call at some point
	;     for now, read in at assembly time, and make a FILE struct
	enter 0, 0
	push  ebx
	push  esi
	;     need a file descriptor, and a ring buffer
	ccall malloc, sizeof.ring_buffer
	test  eax, eax
	jz    .error
	mov   ebx, eax

	mov   esi, .eof - .file
	ccall malloc, esi
	test  eax, eax
	jz    .error

	ccall memcpy, eax, .file, esi

	mov [ring_buffer.length], esi
	mov [ring_buffer.start], 0
	mov [ring_buffer.end], esi
	mov [ring_buffer.capacity], esi
	mov [ring_buffer.buffer], eax

	ccall malloc, sizeof.stdio_file
	test  eax, eax
	jz    .error

	mov [stdio_file.object], ebx
	mov [stdio_file.putc], ring_buffer_putc
	mov [stdio_file.getc], ring_buffer_getc
	mov [stdio_file.ungetc], ring_buffer_ungetc
	mov [stdio_file.termios], NULL
	mov [stdio_file.readkey], NULL
	jmp .return

.error:
	xor eax, eax

.return:
	pop esi
	pop ebx
	leave
	ret

.file:
	file '../../.shellrc'

.eof:
