	format  elf
	include "../includes/debug_symbols.inc"
	section '.text' executable
	;;      32 bit protected mode
	use32
	public  hello_main

	include '../lib/includes/string.inc'
	include '../lib/includes/stdio.inc'

hello_main:
	enter 0, 0
	ccall printf, .hello_str

	leave
	ret

.hello_str: db "Hello world!", 13, 10, 0
