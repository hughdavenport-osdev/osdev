	;; 16 bit real mode
	use16

	public boot

	org MBR_ENTRY

	BUSY_LOOP_SIZE = 100000

	; Bit to set in special CPU register cr0
	PROTECTED_MODE = 00000001b

	; Use below MBR_ENTRY as stack

real_stack_top:

boot:
	; Currently in 16 bit real mode
	; Limitations on real mode is 1MB of memory
	; While in real mode, can access interrupts of the BIOS

	jmp 0x0000:.flushcs; Some BIOS' may load us at 0x0000:0x7C00 while other may load us at 0x07C0:0x0000.
	; Do a far jump to fix this issue, and reload CS to 0x0000.

	MAGIC_OFFSET = boot+0x5 ; 5 bytes for the long jump
	dd magic
	;  used for debugging, 0x7c09
	dw debug_break

.flushcs:
	;    Clear ds if it was set by BIOS to non-0
	mov  ax, 0
	mov  ds, ax
	mov  sp, real_stack_top
	mov  bp, sp
	push bp

	;   Save boot disk in memory
	mov [disk], dl

	call enable_a20
	test ax, ax
	jz   panic16

	;   Not actually needed, but clears screen.
	;   vga works fine in 32 bit mode without this
	mov al, BIOS_VIDEO_MODE_VGA_TEXT
	mov ah, BIOS_VIDEO_SET_MODE
	int BIOS_VIDEO

	call bios_disk_read
	test ax, ax
	jnz  panic16

	jmp second_sector

panic16:
	mov  si, panic16_str
	call bios_print
	;    TODO: print out registers
	;    TODO: print out call stack
	;    TODO: print out memory?
	;    fall through to halt

halt16:
	cli
	hlt
	jmp halt16

	; Used for breakpoints

debug_break:
	xchg bx, bx
	ret

include 'includes/a20.inc'
include 'includes/gdt.inc'
include 'includes/mbr.inc'
include 'includes/bios.inc'

disk:
	db 0x0

panic16_str:
	db "PANIC", 13, 10, 0

display_current_offset:
	bits    = 16
	display 'Max size is 0x7DFE. Current offset is 0x'
	repeat  bits/4
	d       = '0' + $ shr (bits-%*4) and 0Fh
	if      d > '9'
	d       = d + 'A'-'9'-1
	end     if
	display d
	end     repeat
	display 13, 10

.mbr_magic:
	times (MBR_SIZE - 2) - ($-$$) db 0
	dw    MBR_MAGIC

	;; Second 512 byte sector

second_sector:

	call do_e820

	cli

	lgdt [gdt_pointer]

	mov eax, cr0
	or  eax, PROTECTED_MODE
	mov cr0, eax

	mov ax, DATA_SEG
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	jmp CODE_SEG:protected_mode; long jump to the code segment

include 'includes/memory.inc'

protected_mode:
	; fall through, should be boot32 next
