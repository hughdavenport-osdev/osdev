# Hobby osdev project

[![Latest Release](https://gitlab.com/hughdavenport-osdev/osdev/-/badges/release.svg)](https://gitlab.com/hughdavenport-osdev/osdev/-/releases)
[![pipeline status](https://gitlab.com/hughdavenport-osdev/osdev/badges/main/pipeline.svg)](https://gitlab.com/hughdavenport-osdev/osdev/-/commits/main)

[![patreon](https://img.shields.io/badge/patreon-FF5441?style=for-the-badge&logo=Patreon)](Https://www.patreon.com/hughdavenport)
[![youtube](https://img.shields.io/badge/youtube-FF0000?style=for-the-badge&logo=youtube)](https://www.youtube.com/watch?v=Hb37FcfdfJc&list=PL5r5Q39GjMDcAKGSb5L035YrKK0rr8jM1)

## Quick Start

1.  Download the [latest release](https://gitlab.com/hughdavenport-osdev/osdev/-/releases/permalink/latest/downloads/osdev.img).
2.  Go to https://copy.sh/v86/
3.  Next to `Hard drive disk image`, click `Choose File`, and select the `osdev.img` file in the `build` directory.
4.  Click `Start Emulation`

## Demo

![Cool demo!](https://gitlab.com/hughdavenport-osdev/osdev/-/raw/main/demo.gif "Animated GIF showing a demo of the operating system")

You can also watch many live demo's as well as development on my [YouTube playlist](https://www.youtube.com/watch?v=Hb37FcfdfJc&list=PL5r5Q39GjMDcAKGSb5L035YrKK0rr8jM1)

## Contributions

I welcome contributions to the project. See [CONTRIBUTING.md](CONTRIBUTING.md) for more details.

You are welcome to support me financially if you would like on my [patreon](https://www.patreon.com/hughdavenport).

## Dependencies

### Building

You will need the following tools installed

*   `fasm`
*   `gcc` or `clang` (other C compilers may work)
*   `ld`
*   `bash` (`build.sh` uses bashisms, while the rest of the scripts only rely on POSIX `sh`)

The following works on Ubuntu 20.04 LTS:

`sudo apt install fasm build-essential bash`

### Building for debug

If you are debugging, you also need the [porth](https://gitlab.com/tsoding/porth) language installed. The default path it looks for is ~/src/third-party/porth, but this can be overridden with `PORTH_DIR` environment variable.

### Building a release

In addition to the above, you also need:

*   `virtualbox`

The following works on Ubuntu 20.04 LTS:

`sudo apt install virtualbox`

### Testing

If you want to test on a virtual machine, then one of the following need to be installed:

*   `qemu`
*   `virtualbox`
*   `bochs` (slow)

The following commands works on Ubuntu 20.04 LTS:

`sudo apt insall qemu-system-x86`

`sudo apt insall virtualbox`

`sudo apt insall bochs`

### Debugging

If you want to do debugging, you will also need one of the following:

*   `gdb`
*   `bochs` (slow)

The following commands works on Ubuntu 20.04 LTS:

`sudo apt insall gdb`

`sudo apt insall bochs`

## Building

Run the following command to build:
`tools/build.sh`

## Linting

TBD, can see [the CI configuration](.gitlab-ci.yaml) for dependencies

## Building a release

Run the following command to generate an [Open Virtualization Format](https://en.wikipedia.org/wiki/Open_Virtualization_Format) (also known as a `.ova` file):

`tools/build-release.sh`

## Running in a virtual machine

The following commands will automatically build before running the specific virtual machine application.

### Qemu

`tools/run.sh`

### Virtualbox

`tools/run-virtualbox.sh`

### Bochs (slow)

`tools/debug-bochs.sh`
Once a screen shows up, enter `continue` in the terminal.

### v86 (online)

1.  Build the disk (`tools/build.sh`)
2.  Go to https://copy.sh/v86/
3.  Next to `Hard drive disk image`, click `Choose File`, and select the `osdev.img` file in the `build` directory.
4.  Click `Start Emulation`

### Other

Your mileage may vary, but after generating a release, then importing the release `.ova` file into whichever VM application may work. Make a issue in gitlab if you get something working to update this documentation.

## Running on hardware

You need to copy the data from `build/osdev.img` onto a boot disk. This could be a USB pen drive. The hardware needs to support BIOS booting (in the future UEFI will be supported).
Please [report](https://gitlab.com/hughdavenport-osdev/osdev/-/issues/new) any issues you come across.

The following command works on Ubuntu 20.04 LTS (NOTE: replace /dev/sdX with a valid disk):

`tools/build.sh && sudo dd if=build/osdev.img of=/dev/sdX`

## Debugging

The following commands will automatically build, before running the specific debugger.

To create breakpoints, add `int 3` to the source code. Fasm does not support the symbol format used by GDB ([DWARF](https://dwarfstd.org/)), so there are no symbols available. [1][]

### GDB

`tools/debug.sh` (or `tools/debug-gdb.sh`)
Once a screen shows up, enter `continue` in the terminal.

### Bochs (slow)

`tools/debug-bochs.sh`
Once a screen shows up, enter `continue` in the terminal.

### Other

Your mileage may vary, but after building, then the disk file `build/osdev.img` should be a valid bootable x86 disk. Make a issue in gitlab if you get something working to update this documentation.

[1]: https://board.flatassembler.net/topic.php?t=9792
