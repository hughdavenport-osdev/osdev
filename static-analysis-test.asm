callee:
	enter 0, 0

	; ebx, esi, edi should leave as they started
	; eax should get set if non void return

	leave
	ret

caller:
	enter 0, 0

	push esi
	push ebx

	.label xor eax, eax; can have label inline, and comments inline
	.label_no_ops nop; can have label inline, and comments inline
	.label_no_comment xor eax, eax
	.label_no_ops_or_comment nop
	xor    eax, eax; can have comments inline
	nop;   can have comments inline

	mov ecx, 10
	mov edx, 2

.loop:
	test ecx, ecx
	jz   .end

	mov edx, 1

	dec ecx
	jmp .loop

.end:

	call callee

	; ecx, edx are now undefined
	; eax may be undefined or not, would need to analyse callee to see if it is set

	mov ebx, eax

	pop ebx
	pop esi

	leave
	ret
